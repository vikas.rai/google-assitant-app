﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using GoogleAssistantBLL.Enums;
using GoogleAssistantBLL.Intents;
using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GoogleAssistantASPCore
{
    public class GoogleController : Controller
    {
        public static IConfiguration Configuration { get; set; }

        string connectionString = string.Empty;
        string signalRUrl = string.Empty;

        public GoogleController(IConfiguration configuration)
        {
            Configuration = configuration;
            connectionString = Configuration["ConnectionStrings:DefaultConnection"];
            signalRUrl = Configuration["SignalRUrl"];
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Dialogflow()
        {
            string body = string.Empty;

            using (StreamReader reader = new StreamReader(Request.Body))
            {
                body = reader.ReadToEnd();
            }

            Console.WriteLine("Request recieved");

            var header = Request.Headers["authorization"].FirstOrDefault();

            UserModel user = null;
            DialogFlowRequest req = null;

            if (!string.IsNullOrEmpty(header) && "Basic ZGlhbG9nZmxvdzpnb29nbGVhc3Npc3RhbnQ=".Equals(header))
            {
                req = Newtonsoft.Json.JsonConvert.DeserializeObject<DialogFlowRequest>(body);
                user = await GetUserFromRequest(req);
            }

            if (user != null)
            {
                return Json(await ParseIntentAndRespond(user, req));
            }
            else
            {
                return new UnauthorizedResult();
            }
        }

        async Task<dynamic> ParseIntentAndRespond(UserModel user, DialogFlowRequest request)
        {
            Console.WriteLine("sending response");

            IIntentProcessor intentProcessor = null;

            switch (request.QueryResult.Intent.DisplayName.ToLower())
            {
                case "duration.change":
                case "channel.change":
                case "screen.launch":
                case "sentiment.change":
                    intentProcessor = new ChannelChange(signalRUrl, connectionString);
                    break;
                case "twitter.summary":
                    intentProcessor = new TwitterSummary(connectionString);
                    break;
                case "instagram.summary":
                    intentProcessor = new ChannelSummary(connectionString);
                    user.ChannelGroup = ChannelGroupEnum.Instagram;
                    break;
                case "youtube.summary":
                    intentProcessor = new ChannelSummary(connectionString);
                    user.ChannelGroup = ChannelGroupEnum.YouTube;
                    break;
                case "facebook.summary":
                    intentProcessor = new ChannelSummary(connectionString);
                    user.ChannelGroup = ChannelGroupEnum.Facebook;
                    break;
                case "mentions.summary":
                    intentProcessor = new MentionSummary(connectionString); 
                    break;
                case "tickets.summary":
                    intentProcessor = new TicketSummary(connectionString);
                    break;
                case "recommendations":
                    intentProcessor = new RecommendationIntent(connectionString);
                    break;
                case "customer.no":
                    intentProcessor = new NegationIntent();
                    break;
                case "customer.yes":
                    intentProcessor = new AffirmationIntent(Configuration["MailSettings:host"],
                        Configuration["MailSettings:username"], Configuration["MailSettings:password"],
                        int.Parse(Configuration["MailSettings:port"]), bool.Parse(Configuration["MailSettings:isSSL"]), connectionString);

                    break;
                case "top.categories":
                    intentProcessor = new TopCategoryIntent(connectionString);
                    break;
                case "top.hashtags":
                    intentProcessor = new TopKeywordIntent(connectionString);
                    break;
                case "campaign.summary":
                    intentProcessor = new CampaignSummaryIntent(connectionString);
                    break;
                case "campaign.list":
                    intentProcessor = new CampaignListIntent(connectionString);
                    break;
                case "summary":
                    intentProcessor = new SummaryIntent(connectionString);
                    break;
                case "help":
                    intentProcessor = new HelpIntent();
                    break;
                case "default fallback intent":
                    return new { fulfillmentText = $"I am so sorry! I am kind of young and still learning. Can you please rephrase your request?" };
                case "welcome":
                    intentProcessor = new WelcomeIntent();
                    break;
                case "application.end":
                    intentProcessor = new ApplicationEndIntent();
                    break;
                case "show.data.keyword":
                    intentProcessor = new KeywordSearchIntent(signalRUrl, connectionString);
                    break;
                case "reset.filters":
                    intentProcessor = new ResetFilterIntent(signalRUrl);
                    break;
                default:
                    return new { fulfillmentText = $"Sorry I was not able to understand you." };
            }

            try
            {
                var response = await intentProcessor.ProcessAIRequest(request, user);

                return response;
            }
            catch (Exception e)
            {
                string errorFileName = $"error_{DateTime.Today.ToString("yyyyMMMdd")}.log";
                System.IO.File.AppendAllText(errorFileName, e.ToString() + Environment.NewLine + e.StackTrace);

                Console.WriteLine(e.ToString());
                return new { fulfillmentText = "There was an issue in processing your request, please try again later." };
            }
        }

        private async Task<UserModel> GetUserFromRequest(DialogFlowRequest req)
        {
            var authenticationModel = new AuthenticationModel();

            if (req.OriginalDetectIntentRequest != null && req.OriginalDetectIntentRequest.Payload != null &&
                req.OriginalDetectIntentRequest.Payload.User != null)
            {
                return await authenticationModel.VerifyToken(req.OriginalDetectIntentRequest.Payload.User.AccessToken, connectionString);
            }
            else
            {
                return null;
            }
        }

        [HttpGet]
        public async Task<IActionResult> Authorize()
        {
            Console.WriteLine("inside /authorize");
            var clientID = Request.Query["client_id"][0];
            var redirect_uri = Request.Query["redirect_uri"][0];
            string state = string.Empty;

            if (Request.Query["state"].Count > 0)
            {
                state = Request.Query["state"][0];
                HttpContext.Session.SetString("state", state);
            }
                

            var response_type = Request.Query["response_type"][0];
            var scope = Request.Query["scope"][0];

            if (Configuration["client_id"].Equals(clientID) &&
            Configuration["redirect_uri"].Equals(redirect_uri) &&
            "warroom".Equals(scope) &&
            "token".Equals(response_type))
            {
                Console.WriteLine("showing login page");
                ViewBag.State = state;
                return View();
            }
            else
            {
                //return HttpStatusCode.Unauthorized;
                return Content("");
            }
        }

        public async Task<IActionResult> Authenticate()
        {
            var username = Request.Form["txtUsername"];
            var password = Request.Form["txtPassword"];
            var state = Request.Form["State"];

            var authenticationModel = new AuthenticationModel();
            var user = await authenticationModel.Authenticate(username, password, connectionString);

            if (user == null)
            {
                return Content("Invalid user.");
            }
            else
            {
                var token_type = "bearer";

                Console.WriteLine($"returning token: {user.Token}");
                return Redirect($"{Configuration["redirect_uri"]}#access_token={user.Token}&token_type={token_type}&state={state}");
            }
        }
    }
}
