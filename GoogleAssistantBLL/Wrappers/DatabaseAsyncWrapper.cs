﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Wrappers
{
    public class DatabaseAsyncWrapper : IDataAccess
    {
        public async Task<DbTransaction> GetTransaction(string connectionString)
        {
            var connection = await GetConnection(connectionString);
            return await Task.Run(() => connection.BeginTransaction(IsolationLevel.ReadUncommitted));
        }

        async Task<SqlConnection> GetConnection(string connectionString)
        {
            var connection = new SqlConnection(connectionString);
            await connection.OpenAsync();

            return connection;
        }

        SqlCommand GetCommand(DbTransaction transaction, string procedure, Dictionary<string, object> parameters)
        {
            return GetCommand(transaction, procedure, CommandType.StoredProcedure, parameters);
        }

        SqlCommand GetCommand(DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = new SqlCommand(sql, transaction.Connection as SqlConnection, transaction as SqlTransaction);
            command.CommandType = commandType;
            command.CommandTimeout = 3600;

            if (parameters != null)
            {
                foreach (var parameter in parameters.Keys)
                {
                    var name = parameter;
                    var p = new SqlParameter();

                    if (parameter.Contains("o:"))
                    {
                        p.Direction = ParameterDirection.Output;
                        name = parameter.Substring(parameter.IndexOf(':') + 1);
                    }

                    p.ParameterName = name;
                    p.Value = parameters[parameter];

                    command.Parameters.Add(p);
                }


            }
            if (commandType == CommandType.Text)
            {
                command.Prepare();
            }

            return command;
        }

        public async Task<Tuple<bool, ErrorStatusCodes>> ExecuteNonQuery(DbTransaction transaction, string procedure, Dictionary<string, object> parameters)
        {
            bool success = true;
            ErrorStatusCodes error = null;
            var command = GetCommand(transaction, procedure, parameters);

            try
            {
                await command.ExecuteNonQueryAsync();
                FillOutputParams(command, parameters);
                success = true;
            }
            catch (Exception ex)
            {
                error = GetErrorObject(ex);
                success = false;
            }

            return Tuple.Create(success, error);
        }

        public async Task<Tuple<bool, ErrorStatusCodes>> ExecuteNonQuery(DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            bool success = true;
            ErrorStatusCodes error = null;
            var command = GetCommand(transaction, sql, commandType, parameters);

            try
            {
                await command.ExecuteNonQueryAsync();
                FillOutputParams(command, parameters);
                success = true;
            }
            catch (Exception ex)
            {
                error = GetErrorObject(ex);
                success = false;
            }

            return Tuple.Create(success, error);
        }

        public async Task<DbDataReader> GetReader(DbTransaction transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await GetReader(transaction, procedure, CommandType.StoredProcedure, parameters);
        }

        public async Task<DbDataReader> GetReader(DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = GetCommand(transaction, sql, commandType, parameters);

            try
            {
                var reader = await command.ExecuteReaderAsync();
                FillOutputParams(command, parameters);

                return reader;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CloseConnection(DbConnection connection)
        {
            connection.Close();
            connection.Dispose();
            connection = null;
        }

        public void CloseConnection(DbTransaction transaction)
        {
            transaction.Dispose();

            if (transaction.Connection != null)
            {
                transaction.Connection.Close();
                transaction.Connection.Dispose();
            }
        }

        public async Task<List<T>> GetObjectList<T>(DbTransaction transaction, string procedure, Dictionary<string, object> parameters) where T : new()
        {
            return await this.GetObjectList<T>(transaction, procedure, CommandType.StoredProcedure, parameters);
        }

        public async Task<List<T>> GetObjectList<T>(DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters) where T : new()
        {
            List<T> list = new List<T>();
            using (var reader = await this.GetReader(transaction, sql, CommandType.Text, parameters))
            {
                while (reader.Read())
                {
                    T t = new T();
                    reader.MapDataToBusinessEntity(t, false);
                    list.Add(t);
                }

                reader.Close();
            }

            return list;
        }

        public async Task FillObject<T>(T t, DbTransaction transaction, string procedure, Dictionary<string, object> parameters) where T : new()
        {
            await this.FillObject(t, transaction, procedure, CommandType.StoredProcedure, parameters);
        }

        public async Task FillObject<T>(T t, DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters) where T : new()
        {
            using (var reader = await this.GetReader(transaction, sql, CommandType.Text, parameters))
            {
                if (reader.Read())
                {
                    reader.MapDataToBusinessEntity(t, false);
                }

                reader.Close();
            }
        }

        ErrorStatusCodes GetErrorObject(Exception ex)
        {
            ErrorStatusCodes error = new ErrorStatusCodes();
            error.Message = "Database error";
            error.ApiErrorMessage = ex.Message;
            error.StackTrace = ex.StackTrace;

            if (ex is SqlException)
                error.StatusCode = (ex as SqlException).ErrorCode;

            return error;
        }

        void FillOutputParams(SqlCommand command, Dictionary<string, object> parameters)
        {
            foreach (SqlParameter item in command.Parameters)
            {
                if (item.Direction == ParameterDirection.Output)
                {
                    parameters[$"o:{item.ParameterName}"] = item.Value;
                }
            }
        }

        public async Task<object> ExecuteScalar(DbTransaction transaction, string procedure, Dictionary<string, object> parameters)
        {
            var command = GetCommand(transaction, procedure, parameters);
            object res = await command.ExecuteScalarAsync();
            FillOutputParams(command, parameters);
            return res;
        }

        public async Task<object> ExecuteScalar(DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = GetCommand(transaction, sql, commandType, parameters);
            object res = await command.ExecuteScalarAsync();
            FillOutputParams(command, parameters);
            return res;
        }

        public async Task<Tuple<bool, ErrorStatusCodes>> BulkInsert(DbTransaction transaction, string sql, DataTable dt)
        {
            throw new NotImplementedException("not implemented");
            /*
            bool IsSuccess = false;
            ErrorStatusCodes error = null;
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = transaction.Connection as SqlConnection;
                    cmd.Transaction = transaction as SqlTransaction;
                    cmd.CommandText = sql;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.UpdateBatchSize = dt.Rows.Count;
                        using (SqlCommandBuilder cb = new SqlCommandBuilder(da))
                        {
                            await Task.Run(() => da.Update(dt));
                        }

                        IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                error = new ErrorStatusCodes
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                    StatusCode = 100
                };
            }

            return new Tuple<bool, ErrorStatusCodes>(IsSuccess, error);
            */
        }
    }
}
