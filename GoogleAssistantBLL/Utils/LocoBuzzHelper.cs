﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Utils
{
    public static class LocoBuzzHelper
    {
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static double ToUnixTime(DateTime time)
        {
            return time.Subtract(epoch).TotalSeconds;
        }
    }
}
