﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Utils
{
    public class NameAttribute : Attribute
    {
        string Name = string.Empty;

        public NameAttribute(string name)
        {
            this.Name = name;
        }

        public string GetName()
        {
            return this.Name;
        }
    }
}
