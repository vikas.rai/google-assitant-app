﻿using GoogleAssistantBLL.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Utils
{
    public class SignalRUtils
    {
        public static void SendSignalR(string AIParam, UserModel User, string SignalRUrl, string mode = "801")
        {
            string Message = AIParam;
            var client = new RestClient(SignalRUrl);
            var request = new RestRequest("WebHook/Broadcast", RestSharp.Method.POST);
            try
            {
                string JsonData = "{\"Message\":" + Message + ", \"UserID\":\"" + User.UserID + "\"}";
                request.AddParameter("groupID", User.CategoryID, ParameterType.GetOrPost);
                request.AddParameter("message", JsonData, ParameterType.GetOrPost);
                request.AddParameter("UserRole", "Manager", ParameterType.GetOrPost);
                request.AddParameter("Mode", mode, ParameterType.GetOrPost);
                client.Execute(request);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
