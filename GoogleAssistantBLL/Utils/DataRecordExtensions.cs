﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;

namespace GoogleAssistantBLL.Utils
{
    public static class DataRecordExtensions
    {
        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        public static List<T> MapDataToBusinessEntityCollection<T>(this IDataReader dr) where T : new()
        {
            Type businessEntityType = typeof(T);
            List<T> entitys = new List<T>();
            Hashtable hashtable = new Hashtable();
            PropertyInfo[] properties = businessEntityType.GetProperties();
            foreach (PropertyInfo info in properties)
            {
                if (info.GetCustomAttributes(false).Length > 0)
                {
                    var na = ((NameAttribute)info.GetCustomAttributes(false)[0]);
                    hashtable[na.GetName().ToUpper()] = info;
                }
            }

            try
            {
                while (dr.Read())
                {
                    T newObject = new T();
                    for (int index = 0; index < dr.FieldCount; index++)
                    {
                        PropertyInfo info = (PropertyInfo)
                                            hashtable[dr.GetName(index).ToUpper()];
                        if ((info != null) && info.CanWrite)
                        {
                            var dbVal = dr.GetValue(index);

                            if (dbVal.GetType() == info.PropertyType)
                            {
                                if (dbVal != null && dbVal != DBNull.Value && dbVal.GetType() == typeof(DateTime))
                                {
                                    var dt = (DateTime)dbVal;
                                    dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
                                    info.SetValue(newObject, dt, null);
                                }
                                else
                                {
                                    info.SetValue(newObject, dbVal, null);
                                }
                            }
                            else
                                info.SetValue(newObject, null, null);
                        }
                    }
                    entitys.Add(newObject);
                }
            }
            catch (Exception e)
            {
                dr.Close();
                throw e;
            }


            return entitys;
        }

        public static void MapDataToBusinessEntity(this IDataReader dr, object entity)
        {
            MapDataToBusinessEntity(dr, entity, true);
        }

        public static void MapDataToBusinessEntity(this IDataReader dr, object entity, bool ShouldReadTheReader)
        {
            Type businessEntityType = entity.GetType();

            Hashtable hashtable = new Hashtable();
            PropertyInfo[] properties = businessEntityType.GetProperties();
            foreach (PropertyInfo info in properties)
            {
                if (info.GetCustomAttributes(false).Length > 0)
                {
                    var na = ((NameAttribute)info.GetCustomAttributes(false)[0]);
                    hashtable[na.GetName().ToUpper()] = info;
                }
            }

            try
            {
                var shouldIterate = true;

                if (ShouldReadTheReader)
                {
                    shouldIterate = dr.Read();
                }

                if (shouldIterate)
                {
                    for (int index = 0; index < dr.FieldCount; index++)
                    {
                        try
                        {
                            PropertyInfo info = (PropertyInfo)
                                                hashtable[dr.GetName(index).ToUpper()];
                            if ((info != null) && info.CanWrite)
                            {
                                var dbVal = dr.GetValue(index);

                                if (dbVal != DBNull.Value)
                                {
                                    if (info.PropertyType.IsEnum)
                                    {
                                        SetEnumValue(entity, info, dbVal, info.PropertyType);
                                    }
                                    else if (info.PropertyType.Name.Equals("DateTime"))
                                    {
                                        //CultureInfo provider = CultureInfo.GetCultureInfo("en-GB");
                                        var dt = DateTime.SpecifyKind(Convert.ToDateTime(dbVal), DateTimeKind.Utc);

                                        info.SetValue(entity, dt, null);
                                    }
                                    else if (info.PropertyType.IsGenericType && info.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                                    {
                                        var propertyType = info.PropertyType.GetGenericArguments()[0];

                                        if (propertyType.IsEnum)
                                            SetEnumValue(entity, info, dbVal, propertyType);
                                        else if (info.PropertyType.Name.Equals("DateTime"))
                                        {
                                            var dt = DateTime.SpecifyKind(Convert.ToDateTime(dbVal), DateTimeKind.Utc);
                                            info.SetValue(entity, dt, null);
                                        }
                                        else
                                            info.SetValue(entity, Convert.ChangeType(dbVal, propertyType), null);
                                    }
                                    else
                                        info.SetValue(entity, Convert.ChangeType(dbVal, info.PropertyType), null);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                dr.Close();
                throw e;
            }
        }

        private static void SetEnumValue(object entity, PropertyInfo info, object dbVal, Type propertyType)
        {
            if (!string.IsNullOrEmpty(dbVal.ToString()))
            {
                var val = Convert.ChangeType(Enum.ToObject(propertyType, dbVal), propertyType);
                info.SetValue(entity, val, null);
            }
        }
    }
}
