﻿using GoogleAssistantBLL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Interfaces
{
    public interface IDataAccess
    {
        Task<DbTransaction> GetTransaction(string ConnectionString);
        Task<Tuple<bool, ErrorStatusCodes>> ExecuteNonQuery(DbTransaction transaction, string procedure, Dictionary<string, object> parameters);
        Task<Tuple<bool, ErrorStatusCodes>> ExecuteNonQuery(DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters);

        Task<object> ExecuteScalar(DbTransaction transaction, string procedure, Dictionary<string, object> parameters);
        Task<object> ExecuteScalar(DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters);

        Task<DbDataReader> GetReader(DbTransaction transaction, string procedure, Dictionary<string, object> parameters);
        Task<DbDataReader> GetReader(DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters);

        Task<List<T>> GetObjectList<T>(DbTransaction transaction, string procedure, Dictionary<string, object> parameters) where T : new();
        Task<List<T>> GetObjectList<T>(DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters) where T : new();

        Task FillObject<T>(T t, DbTransaction transaction, string procedure, Dictionary<string, object> parameters) where T : new();
        Task FillObject<T>(T t, DbTransaction transaction, string sql, CommandType commandType, Dictionary<string, object> parameters) where T : new();

        Task<Tuple<bool, ErrorStatusCodes>> BulkInsert(DbTransaction transaction, string sql, DataTable dt);

        void CloseConnection(DbConnection connection);
        void CloseConnection(DbTransaction transaction);
    }
}
