﻿using GoogleAssistantBLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Interfaces
{
    public interface IIntentProcessor
    {
        Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user);
    }
}
