﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Enums
{
    public enum UserRoleEnum
    {
        AGENT = 1,
        CustomerCare = 2,
        SupervisorAgent = 3,
        SuperUser = 4,
        PRDashboard = 5,
        AccountConfigurator = 6,
        AnalyticsDashboard = 7,
        BrandAccount = 8,
        ReadOnlySupervisorAgent = 9,
        WarRoom = 10
    }
}
