﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Enums
{
    public enum SentimentType
    {
        Neutral = 0,
        Positive = 1,
        Negative = 2,
        PassivePositive = 3,
        Multiple = 4
    }
}
