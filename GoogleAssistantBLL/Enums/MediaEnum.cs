﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Enums
{
    public enum MediaEnum
    {
        TEXT = 1,
        IMAGE = 2,
        VIDEO = 3,
        URL = 4,
        POLL = 5,
        OTHER = 6,
        ANIMATEDGIF = 7
    }
}
