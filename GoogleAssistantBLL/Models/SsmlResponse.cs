﻿namespace GoogleAssistantBLL.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public partial class SsmlResponse
    {
        public static SsmlResponse GenerateNewInstance
        {
            get
            {
                var response = new SsmlResponse();
                response.Payload = new Payload();
                response.Payload.Google = new Google();
                response.Payload.Google.RichResponse = new RichResponse();
                response.Payload.Google.ExpectUserResponse = true;
                //response.FollowupEventInput = new FollowupEventInput();
                //response.FollowupEventInput.Parameters = new Dictionary<string, string>();
                return response;
            }
        }

        [JsonProperty("payload")]
        public Payload Payload { get; set; }

        //[JsonProperty("followupEventInput")]
        //public FollowupEventInput FollowupEventInput { get; set; }
    }

    public partial class FollowupEventInput
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("languageCode")]
        public string LanguageCode { get; set; }

        [JsonProperty("parameters")]
        public Dictionary<string, string> Parameters { get; set; }
    }

    public partial class Payload
    {
        [JsonProperty("google")]
        public Google Google { get; set; }
    }

    public partial class Google
    {
        [JsonProperty("expectUserResponse")]
        public bool ExpectUserResponse { get; set; }

        [JsonProperty("richResponse")]
        public RichResponse RichResponse { get; set; }
    }

    public partial class RichResponse
    {
        [JsonProperty("items")]
        public Item[] Items { get; set; }
    }

    public partial class Item
    {
        public static Item GetNewInstance
        {
            get
            {
                var item = new Item();
                item.SimpleResponse = new SimpleResponse();
                return item;
            }
        }

        [JsonProperty("simpleResponse")]
        public SimpleResponse SimpleResponse { get; set; }
    }

    public partial class SimpleResponse
    {
        [JsonProperty("ssml")]
        public string Ssml { get; set; }

        [JsonProperty("text_to_speech")]
        public string TextToSpeech { get; set; }

        [JsonProperty("displayText")]
        public string DisplayText { get; set; }
    }

    public partial class SsmlResponse
    {
        public static SsmlResponse FromJson(string json) => JsonConvert.DeserializeObject<SsmlResponse>(json, Converter.Settings);
    }
}
