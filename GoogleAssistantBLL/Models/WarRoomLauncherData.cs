﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Wrappers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Models
{
    public class WarRoomLauncherData
    {
        public async Task<List<WarRoomLauncherModel>> GetWarRoomScreensData(long ID, string ConnectionString)
        {
            List<WarRoomLauncherModel> listModel = new List<WarRoomLauncherModel>();
            try
            {
                IDataAccess db = new DatabaseAsyncWrapper();

                using (var transaction = await db.GetTransaction(ConnectionString))
                {
                    using (var dr = await db.GetReader(transaction, "GetWarRoomScreensData", new Dictionary<string, object>() {
                                        { "@ID", ID}}))
                    {
                        while (dr.Read())
                        {
                            WarRoomLauncherModel model = new WarRoomLauncherModel();
                            model.ID = Convert.ToInt64(dr["ID"]);
                            model.Name = Convert.ToString(dr["Name"]);
                            model.Description = Convert.ToString(dr["Description"]);

                            model.MediaJson = JsonConvert.DeserializeObject<List<WarRoomLauncherMediaJsonModel>>(Convert.ToString(dr["MediaJson"])); //Convert.ToString(dr["MediaJson"]);
                            model.Cost = Convert.ToDecimal(dr["Cost"]);
                            model.IsFree = Convert.ToBoolean(dr["IsFree"]);

                            model.InsertedDate = Convert.ToDateTime(dr["InsertedDate"]);
                            model.HeroImage = Convert.ToString(dr["HeroImage"]);
                            model.LaunchURL = Convert.ToString(dr["LaunchURL"]);
                            model.Rating = Convert.ToInt32(dr["Rating"]);
                            model.PageName = Convert.ToString(dr["PageName"]);
                            model.IsLogicalBrandGroup = Convert.ToBoolean(dr["IsLogicalBrandGroup"]);
                            listModel.Add(model);
                        }

                        dr.Close();
                    }

                    db.CloseConnection(transaction);
                }
            }
            catch (Exception ex)
            {

            }

            return listModel;
        }
    }
}
