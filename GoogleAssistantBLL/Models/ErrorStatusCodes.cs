﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Models
{
    public class ErrorStatusCodes
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }

        public string ApiErrorMessage { get; set; }

        public string FeedbackMessage { get; set; }

        public string StackTrace { get; set; }

        public string AdditionalMessage { get; set; }
    }
}
