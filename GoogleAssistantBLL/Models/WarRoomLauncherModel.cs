﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Models
{
    public class WarRoomLauncherModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<WarRoomLauncherMediaJsonModel> MediaJson { get; set; }
        public Decimal Cost { get; set; }
        public bool IsFree { get; set; }
        public DateTime InsertedDate { get; set; }
        public string HeroImage { get; set; }
        public string LaunchURL { get; set; }
        public int Rating { get; set; }
        public string PageName { get; set; }
        public bool IsLogicalBrandGroup { get; set; }
    }
    public class WarRoomLauncherMediaJsonModel
    {
        public string Path { get; set; }
        public string Type { get; set; }
    }
}
