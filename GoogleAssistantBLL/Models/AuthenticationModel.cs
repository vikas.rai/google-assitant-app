﻿using GoogleAssistantBLL.Enums;
using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Wrappers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Models
{
    public class AuthenticationModel
    {
        public async Task<UserModel> Authenticate(string username, string password, string ConnectionString)
        {
            UserModel user = null;
            IDataAccess db = new DatabaseAsyncWrapper();

            using (var transaction = await db.GetTransaction(ConnectionString))
            {
                bool isUserFound = false;
                password = SHA256(password);

                using (var reader = await db.GetReader(transaction, "RIL_UserLoginDetails", new Dictionary<string, object>() {
                    { "@UserName", username},
                    { "@Password", password }
                }))
                {
                    if (reader.Read())
                    {
                        user = new UserModel();

                        user.UserID = Convert.ToInt64(reader["UserID"]);
                        user.CategoryID = Convert.ToInt64(reader["CategoryID"]);
                        user.CategoryName = Convert.ToString(reader["CategoryName"]);
                        user.Username = username;
                        user.Password = password;
                        user.Name = Convert.ToString(reader["FirstName"]);
                        user.UserRole = (UserRoleEnum)Convert.ToInt32(reader["AccountType"]);
                        user.Token = System.Guid.NewGuid().ToString();
                        user.EmailAddress = Convert.ToString(reader["EmailAddress"]);
                        isUserFound = true;
                    }

                    reader.Close();
                }

                if (isUserFound)
                {
                    var brands = await FetchUserBrands(user, db, transaction);
                    user.Brands = brands;

                    var response = await SaveUserOAuthToken(user, db, transaction);

                    if (response.Item1)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                }

                db.CloseConnection(transaction);

                return user;
            }
        }

        static string SHA256(string randomString)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            try
            {
                byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
                foreach (byte theByte in crypto)
                {
                    hash.Append(theByte.ToString("x2"));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return hash.ToString();
        }

        private async Task<List<Brand>> FetchUserBrands(UserModel user, IDataAccess db, DbTransaction transaction)
        {
            var brands = new List<Brand>();

            using (var reader = await db.GetReader(transaction, "WR_GetUserBrandsByBrandOrder",
                new Dictionary<string, object>() {
                    { "@UserID", user.UserID}
                }))
            {
                while (reader.Read())
                {
                    var b = new Brand();
                    b.BrandFriendlyName = Convert.ToString(reader["BrandFriendlyName"]);
                    b.BrandName = Convert.ToString(reader["BrandName"]);
                    b.BrandID = Convert.ToInt64(reader["BrandID"]);

                    brands.Add(b);
                }

                reader.Close();
            }

            return brands;
        }

        private async Task<Tuple<bool, ErrorStatusCodes>> SaveUserOAuthToken(UserModel user, IDataAccess db, DbTransaction transaction)
        {
            return await db.ExecuteNonQuery(transaction, "WR_SaveGoogleAssistantTokens", new Dictionary<string, object>()
            {
                { "@Userid",  user.UserID},
                { "@UserRole",  (int)user.UserRole},
                { "@Token", user.Token },
                { "@Status", 1 },
                { "@CategoryID", user.CategoryID},
                { "@CategoryName", user.CategoryName},
                { "@Brands", Newtonsoft.Json.JsonConvert.SerializeObject(user.Brands)}
            });
        }

        public async Task<UserModel> VerifyToken(string Token, string ConnectionString)
        {
            UserModel user = null;

            IDataAccess db = new DatabaseAsyncWrapper();

            using (var transaction = await db.GetTransaction(ConnectionString))
            {
                using (var reader = await db.GetReader(transaction, "WR_GetUserByGoogleAssistantToken",
                new Dictionary<string, object>() {
                    { "@token", Token}
                }))
                {
                    if (reader.Read())
                    {
                        user = new UserModel();
                        user.Token = Token;
                        user.UserID = Convert.ToInt64(reader["Userid"]);
                        //user.EmailAddress = Convert.ToString(reader["EmailAddress"]);
                        user.CategoryID = Convert.ToInt64(reader["CategoryID"]);
                        user.Name = Convert.ToString(reader["FirstName"]);
                        user.CategoryName = Convert.ToString(reader["CategoryName"]);
                        user.UserRole = (UserRoleEnum)Convert.ToInt32(reader["UserRole"]);
                        user.TokenGeneratedOn = DateTime.SpecifyKind(Convert.ToDateTime(reader["TokenGeneratedOn"]), DateTimeKind.Utc);
                        user.Brands = JsonConvert.DeserializeObject<List<Brand>>(Convert.ToString(reader["Brands"]));
                    }

                    reader.Close();
                }

                db.CloseConnection(transaction);
            }

            return user;
        }
    }
}
