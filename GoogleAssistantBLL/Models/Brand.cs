﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Models
{
    public class Brand
    {
        public long BrandID { get; set; }
        public string BrandName { get; set; }
        public string BrandFriendlyName { get; set; }
    }
}
