﻿using GoogleAssistantBLL.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Models
{
    public class UserModel
    {
        public string Name { get; set; }
        public long UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string Token { get; set; }
        public string EmailAddress { get; set; }
        public DateTime TokenGeneratedOn { get; set; }
        public UserRoleEnum UserRole { get; set; }

        public List<Brand> Brands { get; set; }
        public ChannelGroupEnum ChannelGroup { get; set; }
    }
}
