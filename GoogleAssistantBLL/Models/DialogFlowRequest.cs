﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoogleAssistantBLL.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class DialogFlowRequest
    {
        [JsonProperty("responseId")]
        public string ResponseId { get; set; }

        [JsonProperty("queryResult")]
        public QueryResult QueryResult { get; set; }

        [JsonProperty("originalDetectIntentRequest")]
        public OriginalDetectIntentRequest OriginalDetectIntentRequest { get; set; }

        [JsonProperty("session")]
        public string Session { get; set; }
    }

    public partial class OriginalDetectIntentRequest
    {
        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("version")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Version { get; set; }

        [JsonProperty("payload")]
        public Payload Payload { get; set; }
    }

    public partial class Payload
    {
        [JsonProperty("isInSandbox")]
        public bool IsInSandbox { get; set; }

        [JsonProperty("surface")]
        public Surface Surface { get; set; }

        [JsonProperty("inputs")]
        public Input[] Inputs { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("conversation")]
        public Conversation Conversation { get; set; }

        [JsonProperty("availableSurfaces")]
        public Surface[] AvailableSurfaces { get; set; }
    }

    public partial class Surface
    {
        [JsonProperty("capabilities")]
        public OutputContext[] Capabilities { get; set; }
    }

    public partial class OutputContext
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class Conversation
    {
        [JsonProperty("conversationId")]
        public string ConversationId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("conversationToken")]
        public string ConversationToken { get; set; }
    }

    public partial class Input
    {
        [JsonProperty("rawInputs")]
        public RawInput[] RawInputs { get; set; }

        [JsonProperty("arguments")]
        public Argument[] Arguments { get; set; }

        [JsonProperty("intent")]
        public string Intent { get; set; }
    }

    public partial class Argument
    {
        [JsonProperty("rawText")]
        public string RawText { get; set; }

        [JsonProperty("textValue")]
        public string TextValue { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class RawInput
    {
        [JsonProperty("query")]
        public string Query { get; set; }

        [JsonProperty("inputType")]
        public string InputType { get; set; }
    }

    public partial class User
    {
        [JsonProperty("lastSeen")]
        public DateTimeOffset LastSeen { get; set; }

        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }
    }

    public partial class QueryResult
    {
        [JsonProperty("queryText")]
        public string QueryText { get; set; }

        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("parameters")]
        public Parameters Parameters { get; set; }

        [JsonProperty("allRequiredParamsPresent")]
        public bool AllRequiredParamsPresent { get; set; }

        [JsonProperty("fulfillmentText")]
        public string FulfillmentText { get; set; }

        [JsonProperty("fulfillmentMessages")]
        public FulfillmentMessage[] FulfillmentMessages { get; set; }

        [JsonProperty("outputContexts")]
        public OutputContext[] OutputContexts { get; set; }

        [JsonProperty("intent")]
        public Intent Intent { get; set; }

        [JsonProperty("intentDetectionConfidence")]
        public long IntentDetectionConfidence { get; set; }

        [JsonProperty("diagnosticInfo")]
        public DiagnosticInfo DiagnosticInfo { get; set; }

        [JsonProperty("languageCode")]
        public string LanguageCode { get; set; }
    }

    public partial class DiagnosticInfo
    {
    }

    public partial class FulfillmentMessage
    {
        [JsonProperty("text")]
        public Text Text { get; set; }
    }

    public partial class Text
    {
        [JsonProperty("text")]
        public string[] TextText { get; set; }
    }

    public partial class Intent
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
    }

    public partial class Parameters
    {
        public Parameters()
        {
            DurationDate = new DatePeriod();
        }

        [JsonProperty("date-period")]
        public DatePeriod DatePeriod { get; set; }
        [JsonProperty("duration")]
        public DateDurationUnit Duration { get; set; }
        [JsonProperty("DurationDate")]
        public DatePeriod DurationDate { get; set; }
        [JsonProperty("channels")]
        public string Channels { get; set; }
        [JsonProperty("dashboard")]
        public string Dashboard { get; set; }
        [JsonProperty("sentiment")]
        public string sentiment { get; set; }
        [JsonProperty("any")]
        public string any { get; set; }
    }
    public class SignalRParameters
    {
        public SignalRParameters()
        {
            Channels = new List<string>();
            Influencers = new List<string>();
            Sentiments = new List<string>();
        }
        public List<string> Channels { get; set; }
        public List<string> Influencers { get; set; }
        public double StartDateEpoch { get; set; }
        public double EndDateEpoch { get; set; }
        public string BrandName { get; set; }
        public List<string> Sentiments { get; set; }
        public AnalyticsKeyword analyticskeyword { get; set; }
    }

    public class AnalyticsKeyword
    {
        public List<string> ShouldContain { get; set; }
        public List<string> AndContain { get; set; }
        public List<string> NotContain { get; set; }
    }

    public class SignalRObj
    {
        public SignalRObj()
        {
            ParamArray = new List<SignalRParameters>();
            LauncherModel = new WarRoomLauncherModel();
        }
        public string Text { get; set; }
        public string PageID { get; set; }
        public List<SignalRParameters> ParamArray { get; set; }
        public WarRoomLauncherModel LauncherModel { get; set; }
        public long BrandID { get; set; }
        public string BrandName { get; set; }
        public bool IsDashboardChange { get; set; }
    }
    public partial class DatePeriod
    {
        [JsonProperty("startDate")]
        public DateTimeOffset StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTimeOffset EndDate { get; set; }
    }
    public class DateDurationUnit
    {
        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }
    }

    public partial class DialogFlowRequest
    {
        public static DialogFlowRequest FromJson(string json) => JsonConvert.DeserializeObject<DialogFlowRequest>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this DialogFlowRequest self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}
