﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class HelpIntent : IIntentProcessor
    {
        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            SsmlResponse resp = SsmlResponse.GenerateNewInstance;
            var item = Item.GetNewInstance;
            
            StringBuilder response = new StringBuilder();
            response.AppendLine($"You can ask me questions like:");
            response.AppendLine("How is my brand doing?");
            response.AppendLine("Brief me about my campaign?");
            response.AppendLine("How am I doing on twitter?");
            response.AppendLine("Launch Tag cloud screen.");
            response.AppendLine("Change duration to 1 week.");
            response.AppendLine("So what can I help you with today?");

            item.SimpleResponse.Ssml = response.ToString();
            resp.Payload.Google.RichResponse.Items = new Item[] { item };

            return resp;
        }
    }
}
