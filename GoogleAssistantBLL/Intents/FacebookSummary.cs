﻿using LocoBuzzRespondDashboardMVCBLL.Interfaces;
using LocoBuzzRespondDashboardMVCBLL.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LocoBuzzRespondDashboardMVCBLL.GoogleAssistant
{
    public class FacebookSummary : IIntentProcessor
    {
        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var FBSummary = await GetFBSummary(user);

            var ImagePost = FBSummary.Media.Where(x => x.MediaEnum == MediaEnum.IMAGE).SingleOrDefault();
            var VideoPost = FBSummary.Media.Where(x => x.MediaEnum == MediaEnum.VIDEO).SingleOrDefault();
            var TextPost = FBSummary.Media.Where(x => x.MediaEnum == MediaEnum.TEXT).SingleOrDefault();

            long TotalPostCount = 0;
            TotalPostCount += ImagePost != null ? ImagePost.Count : 0;
            TotalPostCount += VideoPost != null ? VideoPost.Count : 0;
            TotalPostCount += TextPost != null ? TextPost.Count : 0;

            long MediaPostCount = 0;
            MediaPostCount += ImagePost != null ? ImagePost.Count : 0;
            MediaPostCount += VideoPost != null ? VideoPost.Count : 0;

            long TextPostCount = 0;
            TextPostCount = TextPost != null ? TextPost.Count : 0;


            StringBuilder sbResponse = new StringBuilder();

            sbResponse.AppendLine($"Dear {user.Name}, You have {FBSummary.CurrentPageLikes} page likes. In the last week you acquired {FBSummary.LastWeekNewLikes} likes.");
            sbResponse.AppendLine($"You have done {TotalPostCount} posts in the last 7 days. {MediaPostCount} media and {TextPostCount} text.");
            sbResponse.AppendLine($"You have got {FBSummary.SentimentCount} comments and these are mostly {FBSummary.SentimentType}.");
            sbResponse.AppendLine($"Your mosts successful post was done on {FBSummary.CreatedDate} and has engagement of {FBSummary.EngagementCount}.");

            sbResponse.AppendLine("");
            sbResponse.AppendLine("");

            sbResponse.AppendLine("Do you want us to email you a link to your latest campaign's DIY dashboard?");

            SsmlResponse response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = sbResponse.ToString();
            item.SimpleResponse.DisplayText = Regex.Replace(sbResponse.ToString(), "<.*?>", String.Empty); ;
            response.Payload.Google.RichResponse.Items = new Item[] { item };
            return response;
        }

        public async Task<ChannelSummeryVolume> GetFBSummary(UserModel user)
        {
            IDataAccess db = new DatabaseAsyncWrapper();
            var FacebookSummery = new ChannelSummeryVolume();
            FacebookSummery.Media = new List<MediaEnumVolume>();

            using (var transaction = await db.GetTransaction())
            {
                using (var reader = await db.GetReader(transaction, "GoogleAssistant_ChannelInsight", new Dictionary<string, object>() {
                    { "@CategoryName", user.CategoryName },
                    { "@BrandName", user.Brands.First().BrandName },
                    { "@CategoryID", user.CategoryID },
                    { "@BrandID", user.Brands.First().BrandID },
                    { "@StartDate", DateTime.UtcNow.Date.AddDays(-30).ToUniversalTime() },
                    { "@EndDate", DateTime.UtcNow.Date.Add(new TimeSpan(0,23,59,59,59)).ToUniversalTime() },
                    { "@ChannelGroupID",(int)ChannelGroupEnum.Facebook}
                }))
                {
                    if (reader.Read())
                    {
                        FacebookSummery.CurrentPageLikes = Convert.ToInt64(reader["CurrentPageLikes"]);
                        FacebookSummery.LastWeekNewLikes = Convert.ToInt64(reader["LastWeekNewLikes"]);
                    }

                    await reader.NextResultAsync();

                    while (reader.Read())
                    {
                        var media = new MediaEnumVolume();
                        media.MediaEnum = reader["MediaEnum"] != DBNull.Value ? (MediaEnum)Enum.Parse(typeof(MediaEnum), reader["MediaEnum"].ToString(),true) : MediaEnum.OTHER;
                        media.Count = Convert.ToInt64(reader["Count"]);
                        FacebookSummery.Media.Add(media);
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        FacebookSummery.SentimentType = Convert.ToString(reader["SentimentType"]);
                        FacebookSummery.SentimentCount = Convert.ToInt64(reader["Count"]);
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        FacebookSummery.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                        FacebookSummery.EngagementCount = Convert.ToInt64(reader["Engagement"]);
                    }

                }

                return FacebookSummery;
            }
        }
    }
}
