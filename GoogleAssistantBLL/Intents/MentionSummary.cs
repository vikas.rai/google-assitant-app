﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class MentionSummary : IIntentProcessor
    {
        string connectionString = string.Empty;

        public MentionSummary(string ConnectionString)
        {
            connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var MentionSummary = await GetMentionSummary(user);

            var TotalDiff = MentionSummary.SentimentsCountCurr.Total - MentionSummary.SentimentsCountPrev.Total;
            double TotalPer = MentionSummary.SentimentsCountCurr.Total > 0 ? (TotalDiff / MentionSummary.SentimentsCountPrev.Total) * 100 : 0;
            var chatter = TotalPer > 0 ? "Increasing" : "Decreasing";
            var PositiveDiff = MentionSummary.SentimentsCountCurr.Total - MentionSummary.SentimentsCountPrev.Total;
            double PositivePer = MentionSummary.SentimentsCountCurr.Positive > 0 ? (PositiveDiff / MentionSummary.SentimentsCountPrev.Positive) * 100 : 0;
            var sentiment = PositivePer > 0 ? "up" : "down";

            StringBuilder sbResponse = new StringBuilder();

            sbResponse.AppendLine($"<speak><s>Your overall chatter is {chatter} week on week by {Math.Abs(Math.Round(TotalPer, 2))}%. </s>");
            sbResponse.AppendLine($"<s>Your positive sentiment has gone {sentiment} by {Math.Abs(Math.Round(PositivePer, 2))}% compared to last week.</s>");

            if (MentionSummary.PositiveKewords.Count > 0)
                sbResponse.AppendLine($"<s>This week key positive drivers are:<s>");
            foreach (var PositiveKewords in MentionSummary.PositiveKewords)
            {
                sbResponse.AppendLine($"<s>{PositiveKewords.Key},</s>");
            }

            if (MentionSummary.NegativeKewords.Count > 0)
                sbResponse.AppendLine($"<s>Negative drivers are:</s>");
            foreach (var NegativeKewords in MentionSummary.NegativeKewords)
            {
                sbResponse.AppendLine($"<s>{NegativeKewords.Key},</s>");
            }

            if (MentionSummary.Location.Count > 0)
                sbResponse.AppendLine($"<s>Your key locations are:</s>");
            foreach (var Location in MentionSummary.Location)
            {
                double locationPer = (Location.Value * 100) / MentionSummary.SentimentsCountCurr.Total;
                sbResponse.AppendLine($"<s>{Location.Key} : {Math.Round(locationPer, 2)}%.</s>");
            }

            sbResponse.AppendLine("<break time='200ms'/>");
            sbResponse.AppendLine("Is there anything else I can help you with?</speak>");

            SsmlResponse response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = sbResponse.ToString();
            item.SimpleResponse.DisplayText = Regex.Replace(sbResponse.ToString(), "<.*?>", String.Empty); ;
            response.Payload.Google.RichResponse.Items = new Item[] { item };
            return response;
        }

        public async Task<MentionSummaryVolume> GetMentionSummary(UserModel user)
        {
            IDataAccess db = new DatabaseAsyncWrapper();
            var MentionSummary = new MentionSummaryVolume();

            using (var transaction = await db.GetTransaction(connectionString))
            {
                using (var reader = await db.GetReader(transaction, "GoogleAssistant_Insights", new Dictionary<string, object>() {
                    { "@CategoryName", user.CategoryName },
                    { "@BrandName", user.Brands.First().BrandName },
                    { "@CategoryID", user.CategoryID },
                    { "@BrandID", user.Brands.First().BrandID },
                    { "@StartDate", DateTime.UtcNow.Date.AddDays(-30).ToUniversalTime() },
                    { "@EndDate", DateTime.UtcNow.Date.Add(new TimeSpan(0,23,59,59,59)).ToUniversalTime() }
                }))
                {
                    if (reader.Read())
                    {
                        MentionSummary.SentimentsCountCurr.Neutral = Convert.ToDouble(reader["Neutral"]);
                        MentionSummary.SentimentsCountCurr.Positive = Convert.ToDouble(reader["Positive"]);
                        MentionSummary.SentimentsCountCurr.Negative = Convert.ToDouble(reader["Negative"]);
                        MentionSummary.SentimentsCountCurr.Multiple = Convert.ToDouble(reader["Multiple"]);
                        MentionSummary.SentimentsCountCurr.Total = Convert.ToDouble(reader["Total"]);
                    }

                    await reader.NextResultAsync();

                    while (reader.Read())
                    {
                        MentionSummary.SentimentsCountPrev.Neutral = Convert.ToDouble(reader["Neutral"]);
                        MentionSummary.SentimentsCountPrev.Positive = Convert.ToDouble(reader["Positive"]);
                        MentionSummary.SentimentsCountPrev.Negative = Convert.ToDouble(reader["Negative"]);
                        MentionSummary.SentimentsCountPrev.Multiple = Convert.ToDouble(reader["Multiple"]);
                        MentionSummary.SentimentsCountPrev.Total = Convert.ToDouble(reader["Total"]);
                    }

                    await reader.NextResultAsync();

                    while (reader.Read())
                    {
                        MentionSummary.PositiveKewords.Add(Convert.ToString(reader["PositiveKeyword"]), Convert.ToInt64(reader["Count"]));
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        MentionSummary.NegativeKewords.Add(Convert.ToString(reader["NegativeKeyword"]), Convert.ToInt64(reader["Count"]));
                    }
                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        MentionSummary.Location.Add(Convert.ToString(reader["ContentLocation"]), Convert.ToInt64(reader["Total"]));
                    }

                }

                return MentionSummary;
            }
        }
    }
}
