﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using LocoBuzzRespondDashboardMVCBLL.Interfaces;
using LocoBuzzRespondDashboardMVCBLL.Wrappers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocoBuzzRespondDashboardMVCBLL.GoogleAssistant
{
    public class GoogleRequestLog
    {
        static List<GoogleRequestLog> _logs = null;

        public static List<GoogleRequestLog> Logs
        {
            get
            {
                if (_logs == null)
                {
                    _logs = new List<GoogleRequestLog>();
                }

                return _logs;
            }
        }

        public long RequestID { get; set; }
        public string SessionID { get; set; }
        public DateTime RequestDate { get; set; }
        public bool IsCompleted { get; set; }
        public string IntentName { get; set; }
        public string EventName { get; set; }
        public string SSMLResponse { get; set; }

        public static async Task<GoogleRequestLog> GetGoogleRequestLog(DialogFlowRequest req)
        {
            GoogleRequestLog log = null;

            IDataAccess db = new DatabaseAsyncWrapper();

            using (var transaction = await db.GetTransaction())
            {
                using (var reader = await db.GetReader(transaction, "GetGoogleAssistantRequest",
                    new Dictionary<string, object>() {
                        { "@SessionID", req.Session.Substring(req.Session.LastIndexOf("/")) },
                        { "@IntentName", req.QueryResult.Intent.DisplayName.ToLower() }
                    }))
                {
                    if (reader.Read())
                    {
                        log = new GoogleRequestLog();
                        log.LoadData(reader);
                    }

                    reader.Close();
                }

                db.CloseConnection(transaction);
            }

            return log;
        }

        void LoadData(IDataReader reader)
        {
            if (reader["RequestID"] != DBNull.Value)
                this.RequestID = Convert.ToInt64(reader["RequestID"]);
            if (reader["SessionID"] != DBNull.Value)
                this.SessionID = Convert.ToString(reader["SessionID"]);
            if (reader["RequestDate"] != DBNull.Value)
                this.RequestDate = Convert.ToDateTime(reader["RequestDate"]);
            if (reader["IsCompleted"] != DBNull.Value)
                this.IsCompleted = Convert.ToBoolean(reader["IsCompleted"]);
            if (reader["IntentName"] != DBNull.Value)
                this.IntentName = Convert.ToString(reader["IntentName"]);
            if (reader["TriggerName"] != DBNull.Value)
                this.EventName = Convert.ToString(reader["TriggerName"]);
            if (reader["SSMLResponse"] != DBNull.Value)
                this.SSMLResponse = Convert.ToString(reader["SSMLResponse"]);
        }

        public async Task UpdateGoogleRequestLog()
        {
            IDataAccess db = new DatabaseAsyncWrapper();
            using (var transaction = await db.GetTransaction())
            {
                await db.ExecuteNonQuery(transaction, "", new Dictionary<string, object>() {
                    { "@RequestID", this.RequestID },
                    { "@SSMLResponse", this.SSMLResponse },
                });

                transaction.Commit();
            }

            this.IsCompleted = true;
        }

        public async Task SaveEmptyGoogleRequestLog(DialogFlowRequest req)
        {
            var log = new GoogleRequestLog();

            log.IntentName = req.QueryResult.Intent.DisplayName.ToLower();
            log.EventName = log.IntentName.Replace(".", "_");
            log.IsCompleted = false;
            log.RequestDate = DateTime.UtcNow;
            log.SessionID = req.Session;

            IDataAccess db = new DatabaseAsyncWrapper();
            using (var transaction = await db.GetTransaction())
            {
                try
                {
                    using (var reader = await db.GetReader(transaction, "CreateGoogleAssistantRequest",
                    new Dictionary<string, object>() {
                        { "@SessionID", log.SessionID },
                        { "@RequestDate", log.RequestDate },
                        { "@IsCompleted", log.IsCompleted },
                        { "@IntentName", log.IntentName },
                        { "@TriggerName", log.EventName },
                        { "@SSMLResponse", string.IsNullOrEmpty(log.SSMLResponse) ? DBNull.Value:(object)log.SSMLResponse}
                    }))
                    {
                        if (reader.Read())
                        {
                            log.RequestID = Convert.ToInt64(reader[0]);
                        }

                        reader.Close();
                    }

                    transaction.Commit();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    transaction.Rollback();
                }

                db.CloseConnection(transaction);
            }
        }
    }
}
