﻿using GoogleAssistantBLL.Enums;
using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class TwitterSummary : IIntentProcessor
    {
        string connectionString = string.Empty;

        public TwitterSummary(string ConnectionString)
        {
            this.connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var TwitterSummary = await GettwitterSummary(user);

            var ImagePost = TwitterSummary.Media.Where(x => x.MediaEnum == MediaEnum.IMAGE).SingleOrDefault();
            var VideoPost = TwitterSummary.Media.Where(x => x.MediaEnum == MediaEnum.VIDEO).SingleOrDefault();
            var TextPost = TwitterSummary.Media.Where(x => x.MediaEnum == MediaEnum.TEXT).SingleOrDefault();

            long TotalPostCount = 0;
            TotalPostCount += ImagePost != null ? ImagePost.Count : 0;
            TotalPostCount += VideoPost != null ? VideoPost.Count : 0;
            TotalPostCount += TextPost != null ? TextPost.Count : 0;

            long MediaPostCount = 0;
            MediaPostCount += ImagePost != null ? ImagePost.Count : 0;
            MediaPostCount += VideoPost != null ? VideoPost.Count : 0;

            long TextPostCount = 0;
            TextPostCount = TextPost != null ? TextPost.Count : 0;


            StringBuilder sbResponse = new StringBuilder();

            sbResponse.AppendLine($"<speak><s>You have {NumberUtil.FormatNumber(TwitterSummary.CurrentPageLikes)} followers.</s>");
            sbResponse.AppendLine($"<s>In the last week you acquired {NumberUtil.FormatNumber(TwitterSummary.LastWeekNewLikes)} followers.</s>");
            sbResponse.AppendLine($"<s>You have done {TotalPostCount} tweets in the last 7 days, out of which {MediaPostCount} were media posts and {TextPostCount} were text posts.</s>");
            sbResponse.AppendLine($"<s>You have got around {NumberUtil.FormatNumber(TwitterSummary.SentimentCount)} replies which are mostly {TwitterSummary.SentimentType}.</s>");
            sbResponse.AppendLine($"<s>Your most successful tweet was done on {TwitterSummary.CreatedDate.ToString("dd MMM yyyy hh:mm tt")} which got an engagement of {TwitterSummary.EngagementCount}.</s>");
            sbResponse.AppendLine("<break time='200ms'/>");
            sbResponse.AppendLine("Is there anything else I can help you with?</speak>");

            SsmlResponse response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = sbResponse.ToString();
            item.SimpleResponse.DisplayText = Regex.Replace(sbResponse.ToString(), "<.*?>", String.Empty); ;
            response.Payload.Google.RichResponse.Items = new Item[] { item };
            return response;
        }

        public async Task<ChannelSummeryVolume> GettwitterSummary(UserModel user)
        {
            IDataAccess db = new DatabaseAsyncWrapper();
            var TwitterSummary = new ChannelSummeryVolume();
            TwitterSummary.Media = new List<MediaEnumVolume>();

            using (var transaction = await db.GetTransaction(connectionString))
            {
                using (var reader = await db.GetReader(transaction, "GoogleAssistant_ChannelInsight", new Dictionary<string, object>() {
                    { "@CategoryName", user.CategoryName },
                    { "@BrandName", user.Brands.First().BrandName },
                    { "@CategoryID", user.CategoryID },
                    { "@BrandID", user.Brands.First().BrandID },
                    { "@StartDate", DateTime.UtcNow.Date.AddDays(-30).ToUniversalTime() },
                    { "@EndDate", DateTime.UtcNow.Date.Add(new TimeSpan(0,23,59,59,59)).ToUniversalTime() },
                    { "@ChannelGroupID",(int)ChannelGroupEnum.Twitter}
                }))
                {
                    if (reader.Read())
                    {
                        TwitterSummary.CurrentPageLikes = Convert.ToInt64(reader["CurrentPageLikes"]);
                        TwitterSummary.LastWeekNewLikes = Convert.ToInt64(reader["LastWeekNewLikes"]);
                    }

                    await reader.NextResultAsync();

                    while (reader.Read())
                    {
                        var media = new MediaEnumVolume();
                        media.MediaEnum = reader["MediaEnum"] != DBNull.Value ? (MediaEnum)Enum.Parse(typeof(MediaEnum), reader["MediaEnum"].ToString(), true) : MediaEnum.OTHER;
                        media.Count = Convert.ToInt64(reader["Count"]);
                        TwitterSummary.Media.Add(media);
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        TwitterSummary.SentimentType = Convert.ToString(reader["SentimentType"]);
                        TwitterSummary.SentimentCount = Convert.ToInt64(reader["Count"]);
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        TwitterSummary.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                        TwitterSummary.EngagementCount = Convert.ToInt64(reader["Engagement"]);
                    }

                }

                return TwitterSummary;
            }
        }
    }
}
