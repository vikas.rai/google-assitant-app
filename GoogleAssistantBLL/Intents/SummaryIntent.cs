﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Wrappers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class SummaryIntent : IIntentProcessor
    {
        string connectionString = string.Empty;

        public SummaryIntent(string ConnectionString)
        {
            this.connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var filter = new AssistantFilters();

            if (request.QueryResult.Parameters != null &&
                request.QueryResult.Parameters.DatePeriod != null)
            {
                filter.StartDate = request.QueryResult.Parameters.DatePeriod.StartDate.DateTime;
                filter.EndDate = request.QueryResult.Parameters.DatePeriod.EndDate.DateTime;
            }
            else
            {
                filter.StartDate = DateTime.UtcNow.Date;
                filter.EndDate = DateTime.UtcNow.Date.AddDays(1).Subtract(new TimeSpan(1));
            }

            var response = await GetSummary(filter, user, connectionString);
            double brandTotal = response.Item1.Positive + response.Item1.Negative + response.Item1.Neutral;
            double compTotal = response.Item2.Sum(s => (s.Positive + s.Negative + s.Neutral));

            StringBuilder sbResponse = new StringBuilder();
            sbResponse.Append($"<speak><s>");
            sbResponse.Append($"Total Mentions today are: {NumberUtil.FormatNumber(brandTotal)}.</s>");
            sbResponse.Append($"<s>You have {NumberUtil.FormatNumber(response.Item1.UserCount)} unique users interacting with you.</s>");
            sbResponse.Append($"<s>Today you have acquired {NumberUtil.FormatNumber(response.Item1.TwitterFollowers)} new followers on Twitter and {NumberUtil.FormatNumber(response.Item1.FacebookFollowers)} page likes on facebook.</s>");

            if (response.Item1.InfluencerTypes != null && response.Item1.InfluencerTypes.Count>0)
                sbResponse.Append($"<s>Today you have about {response.Item1.InfluencerTypes.Sum(s => s.Count)} influencers talking about you mainly from {response.Item1.InfluencerTypes.OrderByDescending(o => o.Count).First().CategoryName} category.</s>");

            //sbResponse.Append($"<s>We are listening to { string.Join(", ", response.Item2.Select(s=>s.BrandName).ToList()) } competitors for you and your share of voice indicates you have {((double)(brandTotal/(compTotal+brandTotal))*100).ToString("#.##")}% of overall buzz.</s>");
            //sbResponse.Append($"<s>Currently you have {response.Item3.Count} campaigns  running.</s><s>Last campaign's reach was {NumberUtil.FormatNumber(response.Item3.First().Reach)}, {NumberUtil.FormatNumber(response.Item3.FirstOrDefault().Impression)} impressions and {NumberUtil.FormatNumber(response.Item3.FirstOrDefault().Engagement)} engagement.</s>");

            sbResponse.AppendLine("<break time='200ms'/>");
            sbResponse.AppendLine("Is there anything else I can help you with?</speak>");

            var ssmlResponse = SsmlResponse.GenerateNewInstance;
            Item ssmlItem = Item.GetNewInstance;
            ssmlItem.SimpleResponse.DisplayText = Regex.Replace(sbResponse.ToString(), "<.*?>", String.Empty);
            ssmlItem.SimpleResponse.Ssml = sbResponse.ToString();
            ssmlResponse.Payload.Google.RichResponse.Items = new Item[] { ssmlItem };
            Console.WriteLine("summary response sent");
            return ssmlResponse;
        }

        private async Task<Tuple<DataVolume, List<DataVolume>, List<Campaign>>> GetSummary(AssistantFilters filter, UserModel user, string ConnectionString)
        {
            string errorFileName = $"error_{DateTime.Today.ToString("yyyyMMMdd")}.log";
            var brand = user.Brands.First();

            DataVolume sentimentVolume = null;
            List<DataVolume> compData = null;
            List<Campaign> campaignData = null;

            IDataAccess db = new DatabaseAsyncWrapper();

            using (var transaction = await db.GetTransaction(ConnectionString))
            {
//                File.AppendAllText(errorFileName, $@"
//Category: {user.CategoryName},
//BrandName: {brand.BrandName},
//CategoryID: {user.CategoryID},
//BrandID: {brand.BrandID},
//StartDate: {filter.StartDate.ToString()},
//EndDate: {filter.EndDate.ToString()},
//");

                using (var reader = await db.GetReader(transaction, "GoogleAssistant_Summary", new Dictionary<string, object>()
                {
                    { "@CategoryName", user.CategoryName },
                    { "@BrandName", brand.BrandName },
                    { "@CategoryID", user.CategoryID },
                    { "@BrandId", brand.BrandID },
                    { "@StartDate", filter.StartDate.ToUniversalTime() },
                    { "@EndDate", filter.EndDate.ToUniversalTime() }
                }))
                {
                    try
                    {
                        sentimentVolume = new DataVolume();

                        while (reader.Read())
                        {
                            var sentimentType = Convert.ToInt32(reader["SentimentType"]);

                            switch (sentimentType)
                            {
                                case 0:
                                    sentimentVolume.Neutral = Convert.ToInt64(reader["Count"]);
                                    break;
                                case 1:
                                    sentimentVolume.Positive = Convert.ToInt64(reader["Count"]);
                                    break;
                                case 2:
                                    sentimentVolume.Negative = Convert.ToInt64(reader["Count"]);
                                    break;
                                default:
                                    break;
                            }
                        }

                        await reader.NextResultAsync();

                        if (reader.Read())
                        {
                            if (reader["UniqueUsersInteracting"] != DBNull.Value)
                                sentimentVolume.UserCount = Convert.ToInt64(reader["UniqueUsersInteracting"]);
                        }

                        await reader.NextResultAsync();

                        if (reader.Read())
                        {
                            sentimentVolume.TwitterFollowers = Convert.ToInt64(reader["TwitterNewFollowers"]);
                            sentimentVolume.FacebookFollowers = Convert.ToInt64(reader["FacebookNewPageLikes"]);
                        }

                        await reader.NextResultAsync();

                        sentimentVolume.InfluencerTypes = new List<InfluencerVolume>();

                        while (reader.Read())
                        {
                            var infVolume = new InfluencerVolume();

                            if (reader["InfluencerCategory"] != DBNull.Value)
                                infVolume.CategoryName = reader["InfluencerCategory"].ToString();
                            if (reader["InfluencersCount"] != DBNull.Value)
                                infVolume.Count = Convert.ToInt64(reader["InfluencersCount"]);

                            sentimentVolume.InfluencerTypes.Add(infVolume);
                        }

                        await reader.NextResultAsync();

                        compData = new List<DataVolume>();

                        while (reader.Read())
                        {
                            var dvComp = new DataVolume();
                            dvComp.BrandID = Convert.ToInt64(reader["BrandID"]);
                            dvComp.BrandName = Convert.ToString(reader["BrandFriendlyName"]);
                            dvComp.Negative = Convert.ToInt64(reader["Negative"]);
                            dvComp.Positive = Convert.ToInt64(reader["Positive"]);
                            dvComp.Neutral = Convert.ToInt64(reader["Neutral"]);

                            compData.Add(dvComp);
                        }

                        await reader.NextResultAsync();

                        campaignData = new List<Campaign>();

                        while (reader.Read())
                        {
                            var cData = new Campaign();

                            if (reader["CampaignID"] != DBNull.Value)
                                cData.CampaignID = Convert.ToInt64(reader["CampaignID"]);
                            if (reader["CampaignName"] != DBNull.Value)
                                cData.CampaignName = Convert.ToString(reader["CampaignName"]);
                            if (reader["StartDate"] != DBNull.Value)
                                cData.StartDate = Convert.ToDateTime(reader["StartDate"]);
                            if (reader["EndDate"] != DBNull.Value)
                                cData.EndDate = Convert.ToDateTime(reader["EndDate"]);
                            if (reader["Status"] != DBNull.Value)
                                cData.Status = Convert.ToInt32(reader["Status"]);
                            if (reader["CreatedDate"] != DBNull.Value)
                                cData.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                            if (reader["Reach"] != DBNull.Value)
                                cData.Reach = Convert.ToInt64(reader["Reach"]);
                            if (reader["Impression"] != DBNull.Value)
                                cData.Impression = Convert.ToInt64(reader["Impression"]);
                            if (reader["Engagement"] != DBNull.Value)
                                cData.Engagement = Convert.ToInt64(reader["Engagement"]);

                            campaignData.Add(cData);
                        }

                        reader.Close();
                    }
                    catch (Exception e)
                    {
                        File.AppendAllText(errorFileName, e.ToString() + Environment.NewLine + e.StackTrace);
                    }
                }

                db.CloseConnection(transaction);
            }

            return new Tuple<DataVolume, List<DataVolume>, List<Campaign>>(sentimentVolume, compData, campaignData);
        }
    }
}
