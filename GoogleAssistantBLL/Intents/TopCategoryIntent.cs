﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Wrappers;
using GoogleAssistantBLL.Intents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class TopCategoryIntent : IIntentProcessor
    {
        string connectionString = string.Empty;

        public TopCategoryIntent(string ConnectionString)
        {
            connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var categories = await GetTopCategoriesOrKeywords(connectionString, user, "GoogleAssistant_TopCategories");

            string ssmlText = $"<speak>Top categories this week are:";

            foreach (var cat in categories)
            {
                ssmlText += $"<s>{cat.CategoryName},</s>{Environment.NewLine}";
            }

            ssmlText += $"<break time='200ms'/>" +
                    "Is there anything else I can help you with?</speak>";

            SsmlResponse response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;

            item.SimpleResponse.Ssml = ssmlText;
            response.Payload.Google.RichResponse.Items = new Item[] { item };

            return response;
        }

        public static async Task<List<InfluencerVolume>> GetTopCategoriesOrKeywords(string ConnectionString, 
            UserModel user, string SPName)
        {
            List<InfluencerVolume> volumes = new List<InfluencerVolume>();

            IDataAccess db = new DatabaseAsyncWrapper();
            var TwitterSummary = new ChannelSummeryVolume();
            TwitterSummary.Media = new List<MediaEnumVolume>();

            using (var transaction = await db.GetTransaction(ConnectionString))
            {
                using (var reader = await db.GetReader(transaction, SPName, new Dictionary<string, object>() {
                    { "@CategoryID", user.CategoryID },
                    { "@CategoryName", user.CategoryName },
                    { "@BrandID", user.Brands.First().BrandID },
                    { "@BrandName", user.Brands.First().BrandName }

                }))
                {
                    while (reader.Read())
                    {
                        var infVol = new InfluencerVolume();
                        infVol.CategoryName = Convert.ToString(reader["Name"]);
                        infVol.Count = Convert.ToInt64(reader["Count"]);

                        volumes.Add(infVol);
                    }

                    reader.Close();
                }

                db.CloseConnection(transaction);
            }

            return volumes;
        }
    }
}
