﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Wrappers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class NegationIntent : IIntentProcessor
    {
        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            if (request.QueryResult.OutputContexts != null
                && request.QueryResult.OutputContexts.Count() > 0
                && request.QueryResult.OutputContexts.Any(o => o.Name.Contains("campaignsummary")))
            {
                var response = SsmlResponse.GenerateNewInstance;
                Item item = Item.GetNewInstance;
                item.SimpleResponse.Ssml = $"Ok, Is there anything else I can do for you?";

                response.Payload.Google.RichResponse.Items = new Item[] { item };
                return response;
            }
            else if (request.QueryResult.OutputContexts != null
                && request.QueryResult.OutputContexts.Count() > 0
                && request.QueryResult.OutputContexts.Any(o => o.Name.Contains("welcome")))
            {
                var response = SsmlResponse.GenerateNewInstance;
                Item item = Item.GetNewInstance;
                item.SimpleResponse.Ssml = $"Ok, How may I help you today?";

                response.Payload.Google.RichResponse.Items = new Item[] { item };
                return response;
            }
            else
            {
                return null;
            }
        }
    }

    public class AffirmationIntent : IIntentProcessor
    {
        string host = string.Empty, userName = string.Empty, password = string.Empty, connectionString = string.Empty;
        int port = 25;
        bool isSSL = true;

        public AffirmationIntent(string MailHost, string MailUsername, string MailPassword,
            int MailPort, bool MailIsSSL, string ConnectionString)
        {
            this.host = MailHost;
            this.userName = MailUsername;
            this.password = MailPassword;
            this.port = MailPort;
            this.isSSL = MailIsSSL;
            connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            if (request.QueryResult.OutputContexts != null
                && request.QueryResult.OutputContexts.Count() > 0
                && request.QueryResult.OutputContexts.Any(o=>o.Name.Contains("campaignsummary")))
            {
                var response = await CreateAndSendDIYDashboard(user, connectionString);
                //response.FollowupEventInput = new FollowupEventInput();
                //response.FollowupEventInput.Name = "campaign_summary";

                return response;
            }
            else if (request.QueryResult.OutputContexts != null
                && request.QueryResult.OutputContexts.Count() > 0
                && request.QueryResult.OutputContexts.Any(o => o.Name.Contains("welcome")))
            {
                var intent = new SummaryIntent(connectionString);
                return await intent.ProcessAIRequest(request, user);
            }
            else
            {
                return null;
            }
        }

        private async Task<SsmlResponse> CreateAndSendDIYDashboard(UserModel user, string ConnectionString)
        {
            var GUID = await GetCustomDashboardGUID(user, ConnectionString);

            var response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;

            SmtpClient client = new SmtpClient(host, port);
            client.EnableSsl = isSSL;

            client.Credentials = new NetworkCredential(userName, password);

            var msg = new MailMessage();
            msg.From = new MailAddress("no-reply@locobuzz.com", "LocoBuzz");
            msg.IsBodyHtml = true;

            StringBuilder sbResponse = new StringBuilder();
            sbResponse.AppendLine($"Dear {user.Name},<br/ >");
            sbResponse.AppendLine("This is the link for your campaign's dashboard: ");
            sbResponse.AppendLine("https://social.locobuzz.com/CustomDashBoard/DashBoardDetails?guid=" + GUID + " <br/>");
            sbResponse.AppendLine("Thanks, <br/>");
            sbResponse.AppendLine("LocoBuzz Team.");

            msg.Body = sbResponse.ToString();
            msg.Subject = "Campaign Dashboard URL";
            msg.To.Add(new MailAddress("nitin@locobuzz.com"));

            client.SendAsync(msg, "dfafda");

            //List<string> supressedEmails = new List<string>();

            //EmailWrapper.SendEmail(new List<string>() { "nitin@locobuzz.com" }, null, null, "Campaign Dashboard URL",
            //    sbResponse.ToString(), out supressedEmails);

            item.SimpleResponse.Ssml = $"We have sent a link to DIY dashboard on nitin@locobuzz.com. Is there anything else i can help you with?";
            response.Payload.Google.RichResponse.Items = new Item[] { item };

            return response;
        }

        public async Task<string> GetCustomDashboardGUID(UserModel user, string ConnectionString)
        {
            string GUID = "";
            IDataAccess db = new DatabaseAsyncWrapper();
            var TwitterSummary = new ChannelSummeryVolume();
            TwitterSummary.Media = new List<MediaEnumVolume>();

            using (var transaction = await db.GetTransaction(ConnectionString))
            {
                using (var reader = await db.GetReader(transaction, "InsertDashBoardThoughTalkToGoogle", new Dictionary<string, object>() {
                    { "@CategoryID", user.CategoryID },
                    { "@CategoryName", user.CategoryName },
                    { "@BrandID", user.Brands.First().BrandID },
                    { "@BrandName", user.Brands.First().BrandName },
                    { "@UserID", user.UserID }

                }))
                {
                    if (reader != null)
                    {
                        if (reader.Read())
                        {
                            GUID = Convert.ToString(reader["guid"]);
                        }
                        reader.Close();
                        transaction.Commit();
                    }
                }
            }
            return GUID;
        }
    }
}
