﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class Utils
    {
        public static string TimeAgo(int Days)
        {
            var TimeAgo = "";

            int Years = Days / 365;
            Days = Days % 365;
            int Month = Days / 30;
            Days = Days % 30;

            if (Years > 0)
            {
                TimeAgo = Years + " Years and ";
            }
            if (Month > 0)
            {
                TimeAgo = Month + " Month and ";
            }
            if (Days > 0)
            {
                TimeAgo = Days + " Days";
            }
            if (TimeAgo.EndsWith(" and"))
            {
                TimeAgo = TimeAgo.Remove(TimeAgo.Length - 4);
            }
            return TimeAgo;
        }

    }
}
