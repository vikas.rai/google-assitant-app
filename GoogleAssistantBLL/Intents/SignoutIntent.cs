﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocoBuzzRespondDashboardMVCBLL.GoogleAssistant
{
    public class SignoutIntent : IIntentProcessor
    {
        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            SsmlResponse response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = "Thanks for using LocoBuzz, you have been logged out.";
            item.SimpleResponse.DisplayText = "Thanks for using LocoBuzz, you have been logged out.";
            response.Payload.Google.RichResponse.Items = new Item[] { item };

            return response;
        }
    }
}
