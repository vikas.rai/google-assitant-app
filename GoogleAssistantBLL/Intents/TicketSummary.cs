﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class TicketSummary : IIntentProcessor
    {
        string connectionString = string.Empty;

        public TicketSummary(string ConnectionString)
        {
            connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var TicketSummary = await GetTicketSummary(user);

            var FirstlevelTATinMinutes = TicketSummary.FirstlevelTAT / 60;
            var Total_TATinMinutes = TicketSummary.Total_TAT / 60;
            double DirectlyClosedPer = (TicketSummary.DirectlyClosed * 100) / TicketSummary.TotalClosed;
            double EscalatedClosedPer = (TicketSummary.EscalatedClosed * 100) / TicketSummary.TotalClosed;

            StringBuilder sbResponse = new StringBuilder();

            sbResponse.AppendLine($"<speak><s>You got {TicketSummary.Ticket} Tickets in 7 days.</s>");
            sbResponse.AppendLine($"<s>You have closed {TicketSummary.Closed} Tickets and");
            sbResponse.AppendLine($"{TicketSummary.Pending} Tickets are pending.</s>");
            sbResponse.AppendLine($"<s>Your first level TAT is {FirstlevelTATinMinutes.ToString("#.##")} minutes.</s>");
            sbResponse.AppendLine($"<s>Your TAT to closure is {Total_TATinMinutes.ToString("#.##")} minutes.</s>");
            sbResponse.AppendLine($"<s>You have {TicketSummary.Total_TAT.ToString("#.##")} people responding and have close {Math.Round(DirectlyClosedPer, 2)}%  directly and {Math.Round(EscalatedClosedPer, 2)}% post escalation.</s>");

            sbResponse.AppendLine("<break time='200ms'/>");
            sbResponse.AppendLine("Is there anything else I can help you with?</speak>");

            SsmlResponse response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = sbResponse.ToString();
            item.SimpleResponse.DisplayText = Regex.Replace(sbResponse.ToString(), "<.*?>", String.Empty); ;
            response.Payload.Google.RichResponse.Items = new Item[] { item };
            return response;
        }

        public async Task<TicketSummaryVolume> GetTicketSummary(UserModel user)
        {
            IDataAccess db = new DatabaseAsyncWrapper();
            var TicketSummary = new TicketSummaryVolume();

            using (var transaction = await db.GetTransaction(connectionString))
            {
                using (var reader = await db.GetReader(transaction, "GoogleAssistant_CS", new Dictionary<string, object>() {
                    { "@CategoryName", user.CategoryName },
                    { "@BrandName", user.Brands.First().BrandName },
                    { "@CategoryID", user.CategoryID },
                    { "@BrandID", user.Brands.First().BrandID },
                    { "@StartDate", DateTime.UtcNow.Date.AddDays(-30).ToUniversalTime() },
                    { "@EndDate", DateTime.UtcNow.Date.Add(new TimeSpan(0,23,59,59,59)).ToUniversalTime() }
                }))
                {
                    if (reader.Read())
                    {
                        TicketSummary.Ticket = Convert.ToDouble(reader["Ticket"]);
                        TicketSummary.Mention = Convert.ToDouble(reader["Mention"]);
                        TicketSummary.Closed = Convert.ToDouble(reader["Closed"]);
                        TicketSummary.Pending = Convert.ToDouble(reader["Pending"]);
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        TicketSummary.FirstlevelTAT = Convert.ToDouble(reader["FirstlevelTAT"]);
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        TicketSummary.Total_TAT = Convert.ToDouble(reader["Total_TAT"]);
                    }
                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        TicketSummary.UniqueUsers = Convert.ToDouble(reader["UniqueUsers"]);
                        TicketSummary.TotalClosed = Convert.ToDouble(reader["TotalClosed"]);
                        TicketSummary.DirectlyClosed = Convert.ToDouble(reader["DirectlyClosed"]);
                        TicketSummary.ReplyClosed = Convert.ToDouble(reader["ReplyClosed"]);
                        TicketSummary.EscalatedClosed = Convert.ToDouble(reader["EscalatedClosed"]);
                    }

                }

                return TicketSummary;
            }
        }
    }
}
