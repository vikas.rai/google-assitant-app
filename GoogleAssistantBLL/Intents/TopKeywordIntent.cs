﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class TopKeywordIntent: IIntentProcessor
    {
        string connectionString = string.Empty;

        public TopKeywordIntent(string ConnectionString)
        {
            connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var categories = await TopCategoryIntent.GetTopCategoriesOrKeywords(connectionString, user, "GoogleAssistant_TopHashTags");

            string ssmlText = $"<speak>Top Keywords this week are:";

            foreach (var cat in categories)
            {
                ssmlText += $"<s>{cat.CategoryName},</s>{Environment.NewLine}";
            }

            ssmlText += $"<break time='200ms'/>" +
                    "Is there anything else I can help you with?</speak>";

            SsmlResponse response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;

            item.SimpleResponse.Ssml = ssmlText;
            response.Payload.Google.RichResponse.Items = new Item[] { item };

            return response;
        }
    }
}
