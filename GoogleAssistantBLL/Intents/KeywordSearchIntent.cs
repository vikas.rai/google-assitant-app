﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class KeywordSearchIntent : IIntentProcessor
    {
        string SignalRUrl = string.Empty;
        string connectionString = string.Empty;

        public KeywordSearchIntent(string SignalRUrl, string ConnectionString)
        {
            this.SignalRUrl = SignalRUrl;
            this.connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var keyword = request.QueryResult.Parameters.any;

            SignalRParameters sp = new SignalRParameters();
            sp.BrandName = user.Brands.FirstOrDefault().BrandName;
            sp.analyticskeyword = new AnalyticsKeyword();
            sp.analyticskeyword.ShouldContain = new List<string>();
            sp.analyticskeyword.ShouldContain.Add(keyword);

            WarRoomLauncherData wd = new WarRoomLauncherData();
            SignalRObj Obj = new SignalRObj();
            Obj.ParamArray.Add(sp);
            Obj.Text = keyword;
            SignalRUtils.SendSignalR(JsonConvert.SerializeObject(Obj), user, SignalRUrl);

            var response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = $"<speak><s>Showing data around {Obj.Text} keyword and waiting for further instructions.</s></speak>";
            response.Payload.Google.RichResponse.Items = new Item[] { item };
            response.Payload.Google.ExpectUserResponse = true;

            return response;
        }
    }
}
