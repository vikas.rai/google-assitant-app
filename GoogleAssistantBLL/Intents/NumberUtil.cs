﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class NumberUtil
    {
        public static string FormatNumber(double n)
        {
            if (n < 1000)
                return n.ToString();

            if (n < 10000)
                return String.Format("{0:#,.##} thousand", n - 5);

            if (n < 100000)
                return String.Format("{0:#,.#} thousand", n - 50);

            if (n < 1000000)
                return String.Format("{0:#,.} thousand", n - 500);

            if (n < 10000000)
                return String.Format("{0:#,,.##} million", n - 5000);

            if (n < 100000000)
                return String.Format("{0:#,,.#} million", n - 50000);

            if (n < 1000000000)
                return String.Format("{0:#,,.} million", n - 500000);

            return String.Format("{0:#,,,.##} billion", n - 5000000);
        }
    }
}
