﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class ResetFilterIntent : IIntentProcessor
    {
        private string SignalRUrl;

        public ResetFilterIntent(string SignalRUrl)
        {
            this.SignalRUrl = SignalRUrl;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            SignalRParameters sp = new SignalRParameters();
            sp.BrandName = user.Brands.FirstOrDefault().BrandName;

            WarRoomLauncherData wd = new WarRoomLauncherData();
            SignalRObj Obj = new SignalRObj();
            SignalRUtils.SendSignalR(JsonConvert.SerializeObject(Obj), user, SignalRUrl, "802");

            var response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = $"<speak><s>I have resetted the filters and waiting for further instructions.</s></speak>";
            response.Payload.Google.RichResponse.Items = new Item[] { item };
            response.Payload.Google.ExpectUserResponse = true;

            return response;
        }
    }
}
