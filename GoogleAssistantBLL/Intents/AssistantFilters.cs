﻿using GoogleAssistantBLL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class AssistantFilters
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Brand SelectedBrand { get; set; }
        public List<ChannelGroupEnum> Channels { get; set; }
        public List<string> Influencers { get; set; }
        public SentimentType? Sentiment { get; set; }
    }
}
