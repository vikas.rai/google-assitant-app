﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocoBuzzRespondDashboardMVCBLL.GoogleAssistant
{
    public class WaitIntent : IIntentProcessor
    {
        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var response = SsmlResponse.GenerateNewInstance;
            //response.FollowupEventInput = new FollowupEventInput();
            //response.FollowupEventInput.Name = request.QueryResult.Intent.DisplayName.ToLower().Replace(".", "_");

            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = @"
<speak>
    <prosody rate='slow'>Please wait while we are processing your request.</prosody>
</speak>
";
            response.Payload.Google.RichResponse.Items = new Item[] { item };
            return response;
        }
    }
}
