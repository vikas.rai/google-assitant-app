﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class RecommendationIntent: IIntentProcessor
    {
        string connectionString = String.Empty;

        public RecommendationIntent(string ConnectionString)
        {
            this.connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var trends = await GetTodayTrends(user);

            var topics = string.Join(", <break time='100ms'/>", trends.Select(s => s.Name).ToList());

            var now = DateTime.UtcNow.AddMinutes(330);
            var timeRecommendation = "";

            if (now <= DateTime.UtcNow.Date.Add(new TimeSpan(17, 45, 0)))
            {
                timeRecommendation = "by 6 PM today";
            }
            else
            {
                timeRecommendation = "first thing tomorrow morning";
            }

            string res = $"<speak>" +
                $"<s>It has been seen that a video post on your facebook page generates more interactions than average.</s>" +
                $"<s>So LocoBuzz recommends that you should post a video on any of the following topics: <break time='200ms'/> {topics} on your facebook page " +
                $"{timeRecommendation}.</s>";

            res += Environment.NewLine + "<break time='200ms'/>" +
                    "Is there anything else I can help you with?</speak>";

            SsmlResponse response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = res;

            response.Payload.Google.RichResponse.Items = new Item[] { item };
            return response;
        }

        public async Task<List<Location>> GetTodayTrends(UserModel user)
        {
            var locations = new List<Location>();

            IDataAccess db = new DatabaseAsyncWrapper();

            using (var trans = await db.GetTransaction(this.connectionString))
            {
                using (var reader = await db.GetReader(trans, "GetTopTrendingHashtagsToday_Nitin",
                    new Dictionary<string, object>()))
                {
                    while (reader.Read())
                    {
                        var location = new Location() { Name = Convert.ToString(reader["trendtopic"]).Replace("#", string.Empty) };
                        locations.Add(location);
                    }

                    reader.Close();
                }

                db.CloseConnection(trans);
            }

            return locations.Take(5).ToList();
        }
    }
}
