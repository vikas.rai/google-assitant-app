﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class CampaignSummaryIntent : IIntentProcessor
    {
        string connectionString = string.Empty;

        public CampaignSummaryIntent(string ConnectionString)
        {
            connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var result = await GetCampaignSummary(user);
            var Campaign = result.Item1;
            var TotalVolume = result.Item2;
            var Locations = result.Item3;

            var Sentiment = Campaign.PositiveCount > 0 ? "Positive" : Campaign.NegativeCount > 0 ? "Negative" : Campaign.NeutralCount > 0 ? "Neutral" : "";
            var SentimentCount = Campaign.PositiveCount > 0 ? Campaign.PositiveCount : Campaign.NegativeCount > 0 ? Campaign.NegativeCount : Campaign.NeutralCount > 0 ? Campaign.NeutralCount : 0;
            var TopLocation = Locations != null ? string.Join(", ", Locations.Select(x => x.Name).ToArray().Take(3)) : "";
            double ChatterPer = (Campaign.TotalPosts / TotalVolume.Total) * 100;
            double followerAcquisitionPer = (Campaign.NewFans / Campaign.PCount) * 100;

            double DiffAvg = Campaign.CurrentAvg - Campaign.PrevAvg;
            double PerAvg = Campaign.PrevAvg > 0 ? (DiffAvg / Campaign.PrevAvg) * 100 : 0;

            StringBuilder sbResponse = new StringBuilder();

            sbResponse.AppendLine($"<speak><s>Your latest campaign is running for {Utils.TimeAgo(Campaign.DateDiff)}.</s>");
            sbResponse.AppendLine($"<s>It has contributed to {Math.Round(ChatterPer, 2)}% of overall chatter with {Campaign.TotalPosts} mentions, it has increased follower acquisition by {Math.Round(followerAcquisitionPer, 2)}%.</s>");
            sbResponse.AppendLine($"<s>Sentiment is largely {Sentiment}, it has attracted {Campaign.InfluencerCount} influencers.</s>");
            //sbResponse.AppendLine($"Top 3 locations were agra, alwar and allahbad.");
            sbResponse.AppendLine($"<s>Top 3 locations were {TopLocation}.</s>");
            sbResponse.AppendLine($"<s>Compared to your last campaign it is doing {Math.Round(PerAvg, 2)}% more chatter on a daily average.</s>");

            sbResponse.AppendLine("<break time='200ms'/>");
            sbResponse.AppendLine("Do you want us to email you a link to your latest campaign's DIY dashboard?</speak>");

            SsmlResponse response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = sbResponse.ToString();
            item.SimpleResponse.DisplayText = Regex.Replace(sbResponse.ToString(), "<.*?>", String.Empty); ;
            response.Payload.Google.RichResponse.Items = new Item[] { item };
            //return sbResponse.ToString();
            return response;
        }

        public async Task<Tuple<Campaign, DataVolume, List<Location>>> GetCampaignSummary(UserModel user)
        {
            IDataAccess db = new DatabaseAsyncWrapper();
            List<CampaignAvg> lstCampaignAvg = new List<CampaignAvg>();
            var campaign = new Campaign();
            var totalVolume = new DataVolume();
            var locations = new List<Location>();

            using (var transaction = await db.GetTransaction(connectionString))
            {
                using (var reader = await db.GetReader(transaction, "GoogleAssistant_Campaign", new Dictionary<string, object>() {
                    { "@CategoryName", user.CategoryName },
                    { "@BrandName", user.Brands.First().BrandName },
                    { "@CategoryID", user.CategoryID },
                    { "@BrandID", user.Brands.First().BrandID },
                    { "@StartDate", DateTime.UtcNow.Date.AddDays(-30).ToUniversalTime() },
                    { "@EndDate", DateTime.UtcNow.Date.Add(new TimeSpan(0,23,59,59,59)).ToUniversalTime() }
                }))
                {
                    if (reader.Read())
                    {
                        campaign.CampaignID = Convert.ToInt64(reader["CampaignID"]);
                        campaign.CampaignName = Convert.ToString(reader["CampaignName"]);
                        campaign.StartDate = Convert.ToDateTime(reader["StartDate"]);
                        campaign.EndDate = Convert.ToDateTime(reader["Enddate"]);
                        campaign.Status = Convert.ToInt32(reader["Status"]);
                        campaign.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                        campaign.DateDiff = Convert.ToInt32(reader["DateDifference"]);
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        campaign.TotalPosts = Convert.ToDouble(reader["CampaignCount"]);
                        totalVolume.Total = Convert.ToDouble(reader["TotalCount"]);
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        campaign.NewFans = reader["NewFans"] != DBNull.Value ? Convert.ToDouble(reader["NewFans"]) : 0;
                        campaign.FollowersCount = reader["FollowersCount"] != DBNull.Value ? Convert.ToInt64(reader["FollowersCount"]) : 0;
                        campaign.PCount = reader["PCount"] != DBNull.Value ? Convert.ToDouble(reader["PCount"]) : 0;
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        var sentimentType = Convert.ToInt32(reader["SentimentType"]);

                        switch (sentimentType)
                        {
                            case 0:
                                campaign.NeutralCount = Convert.ToInt64(reader["TopSentimentCount"]);
                                break;
                            case 1:
                                campaign.PositiveCount = Convert.ToInt64(reader["TopSentimentCount"]);
                                break;
                            case 2:
                                campaign.NegativeCount = Convert.ToInt64(reader["TopSentimentCount"]);
                                break;
                            default:
                                break;
                        }
                    }

                    await reader.NextResultAsync();

                    if (reader.Read())
                    {
                        campaign.InfluencerCount = Convert.ToInt64(reader["NewAttractedInfluencers"]);
                    }

                    await reader.NextResultAsync();

                    while (reader.Read())
                    {
                        var location = new Location();

                        if (reader["ContentLocation"] != DBNull.Value)
                            location.Name = Convert.ToString(reader["ContentLocation"]);
                        if (reader["Count"] != DBNull.Value)
                            location.Count = Convert.ToInt64(reader["Count"]);

                        locations.Add(location);
                    }

                    await reader.NextResultAsync();

                    while (reader.Read())
                    {
                        var CampaignAvg = new CampaignAvg();

                        CampaignAvg.CampaignID = reader["CampaignID"] != DBNull.Value ? Convert.ToInt64(reader["CampaignID"]) : 0;
                        CampaignAvg.Avg = reader["Avg"] != DBNull.Value ? Convert.ToDouble(reader["Avg"]) : 0;

                        lstCampaignAvg.Add(CampaignAvg);
                    }

                    reader.Close();
                }

                db.CloseConnection(transaction);
                if (lstCampaignAvg.Count > 0)
                    campaign.CurrentAvg = lstCampaignAvg[0].Avg;
                if (lstCampaignAvg.Count > 1)
                    campaign.PrevAvg = lstCampaignAvg[1].Avg;
            }

            return new Tuple<Campaign, DataVolume, List<Location>>(campaign, totalVolume, locations);
        }
    }
}
