﻿using GoogleAssistantBLL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class DataVolume
    {
        public string BrandName { get; set; }
        public long BrandID { get; set; }
        public double Total { get; set; }
        public long Positive { get; set; }
        public long Negative { get; set; }
        public long Neutral { get; set; }

        public long UserCount { get; set; }
        public long TwitterFollowers { get; set; }
        public long FacebookFollowers { get; set; }
        public List<InfluencerVolume> InfluencerTypes { get; set; }

        public DateTime CreatedDate { get; set; }

        public long Engagement { get; set; }
    }

    public class Campaign
    {
        public long CampaignID { get; set; }
        public string CampaignName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }

        public long Reach { get; set; }
        public long Impression { get; set; }
        public double TotalPosts { get; set; }
        public long Engagement { get; set; }

        public long InfluencerCount { get; set; }

        public long PositiveCount { get; set; }
        public long NegativeCount { get; set; }
        public long NeutralCount { get; set; }

        public double NewFans { get; set; }
        public long FollowersCount { get; set; }
        public double PCount { get; set; }

        public double CurrentAvg { get; set; }
        public double PrevAvg { get; set; }
        public int DateDiff { get; set; }

    }

    public class InfluencerVolume
    {
        public string CategoryName { get; set; }
        public long Count { get; set; }
    }

    public class Location
    {
        public string Name { get; set; }
        public long Count { get; set; }
    }
    public class CampaignAvg
    {
        public long CampaignID { get; set; }
        public double Avg { get; set; }
    }
    public class ChannelSummeryVolume
    {
        public long CurrentPageLikes { get; set; }
        public long LastWeekNewLikes { get; set; }
        public List<MediaEnumVolume> Media { get; set; }
        public string SentimentType { get; set; }
        public long SentimentCount { get; set; }
        public DateTime CreatedDate { get; set; }
        public long EngagementCount { get; set; }

    }
    public class MediaEnumVolume
    {
        public MediaEnum MediaEnum { get; set; }
        public long Count { get; set; }
    }
    public class TicketSummaryVolume
    {
        public double Ticket { get; set; }
        public double Mention { get; set; }
        public double Closed { get; set; }
        public double Pending { get; set; }
        public double FirstlevelTAT { get; set; }
        public double Total_TAT { get; set; }
        public double UniqueUsers { get; set; }
        public double TotalClosed { get; set; }
        public double DirectlyClosed { get; set; }
        public double ReplyClosed { get; set; }
        public double EscalatedClosed { get; set; }
    }
    public class SentimentsCountVolume
    {
        public double Neutral { get; set; }
        public double Positive { get; set; }
        public double Negative { get; set; }
        public double Multiple { get; set; }
        public double Total { get; set; }
    }
    public class MentionSummaryVolume
    {
        public MentionSummaryVolume()
        {
            SentimentsCountCurr = new SentimentsCountVolume();
            SentimentsCountPrev = new SentimentsCountVolume();
            PositiveKewords = new Dictionary<string, long>();
            NegativeKewords = new Dictionary<string, long>();
            Location = new Dictionary<string, long>();
        }
        public SentimentsCountVolume SentimentsCountCurr { get; set; }
        public SentimentsCountVolume SentimentsCountPrev { get; set; }
        public Dictionary<string, long> PositiveKewords { get; set; }
        public Dictionary<string, long> NegativeKewords { get; set; }
        public Dictionary<string, long> Location { get; set; }
    }

}
