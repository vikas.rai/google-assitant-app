﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class ChannelChange : IIntentProcessor
    {
        string SignalRUrl = string.Empty;
        string connectionString = string.Empty;

        public ChannelChange(string SignalRUrl, string ConnectionString)
        {
            this.SignalRUrl = SignalRUrl;
            this.connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var BrandDetails = user.Brands.FirstOrDefault();
            DateTime StartDate = DateTime.MinValue;
            DateTime EndDate = DateTime.MinValue;

            var PageID = 0;
            var IsDashboardChange = false;
            var Params = request.QueryResult.Parameters;
            SignalRParameters SignalRParams = new SignalRParameters();

            if (Params != null)
            {
                if (!string.IsNullOrEmpty(Params.Channels))
                {
                    SignalRParams.Channels.Add(Params.Channels);
                }
                if (!string.IsNullOrEmpty(Params.sentiment))
                {
                    SignalRParams.Sentiments.Add(Params.sentiment);
                }
                //if (user.Brands != null && user.Brands.Count > 0)
                //{
                //    SignalRParams.BrandName=BrandDetails.BrandName;
                //}

                if (Params.DatePeriod != null && Params.DatePeriod.StartDate != null && Params.DatePeriod.EndDate != null)
                {
                    StartDate = Params.DatePeriod.StartDate.Date;
                    EndDate = Params.DatePeriod.EndDate.Date;
                    SignalRParams.StartDateEpoch = LocoBuzzHelper.ToUnixTime(Params.DatePeriod.StartDate.Date);
                    SignalRParams.EndDateEpoch = LocoBuzzHelper.ToUnixTime(Params.DatePeriod.EndDate.Date);
                }
                else if (Params.Duration != null)
                {
                    GetDateFromString(ref Params);
                    StartDate = Params.DurationDate.StartDate.Date;
                    EndDate = Params.DurationDate.EndDate.Date;
                    SignalRParams.StartDateEpoch = LocoBuzzHelper.ToUnixTime(Params.DurationDate.StartDate.Date);
                    SignalRParams.EndDateEpoch = LocoBuzzHelper.ToUnixTime(Params.DurationDate.EndDate.Date);
                }

                if (!string.IsNullOrEmpty(Params.Dashboard))
                {
                    switch (Params.Dashboard)
                    {
                        case "Tag Cloud":
                            PageID = 14;
                            break;
                        case "Campaign":
                            PageID = 11;
                            break;
                        case "Share of Voice":
                            PageID = 6;
                            break;
                        case "Heart Beat":
                            PageID = 1;
                            break;
                        case "PR Dashboard":
                            PageID = 2;
                            break;
                        case "Influencers":
                            PageID = 4;
                            break;
                        case "Brand Post":
                            PageID = 22;
                            break;
                        case "3-D Globe":
                            PageID = 7;
                            break;
                        case "Stream":
                            PageID = 9;
                            break;
                        case "Influencer Stream":
                            PageID = 19;
                            break;
                        case "Category":
                            PageID = 13;
                            break;
                        case "SocialTrend":
                            PageID = 18;
                            break;
                        case "Post Gallery":
                            PageID = 20;
                            break;
                        case "Trend":
                            PageID = 21;
                            break;
                        case "Google Analytics":
                            PageID = 23;
                            break;
                        case "Twitter Trends":
                            PageID = 24;
                            break;
                        case "Google Trends":
                            PageID = 25;
                            break;
                        default:
                            break;
                    }
                    IsDashboardChange = true;
                }
                else if (request.QueryResult.QueryText.Contains("launch") && !IsDashboardChange)
                {
                    PageID = 6;
                    IsDashboardChange = true;
                }
            }
            WarRoomLauncherData wd = new WarRoomLauncherData();
            SignalRObj Obj = new SignalRObj();
            Obj.LauncherModel = (await wd.GetWarRoomScreensData(PageID, connectionString)).FirstOrDefault();
            Obj.ParamArray.Add(SignalRParams);
            Obj.BrandID = BrandDetails.BrandID;
            Obj.BrandName = BrandDetails.BrandName;
            Obj.IsDashboardChange = IsDashboardChange;
            SignalRUtils.SendSignalR(JsonConvert.SerializeObject(Obj), user, SignalRUrl);
            StringBuilder sbResponse = new StringBuilder();

            var msg = "";
            if (Obj.ParamArray.Count > 0 && Obj.ParamArray[0].Sentiments.Count > 0 && !string.IsNullOrEmpty(Obj.ParamArray[0].Sentiments[0]))
            {
                msg = Obj.ParamArray[0].Sentiments[0];
            }
            else if (Obj.ParamArray.Count > 0 && Obj.ParamArray[0].Channels.Count > 0 && !string.IsNullOrEmpty(Obj.ParamArray[0].Channels[0]))
            {
                msg = Obj.ParamArray[0].Channels[0];
            }
            else if (StartDate != DateTime.MinValue && EndDate != DateTime.MinValue)
            {
                msg = StartDate.Date.ToString("dd MMMM yyyy") + " to " + EndDate.Date.ToString("dd MMMM yyyy");
            }

            sbResponse.AppendLine("Ok, Showing " + msg + $" data on {Params.Dashboard} screen and waiting for further instructions.");
            sbResponse.AppendLine("");

            SsmlResponse response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = sbResponse.ToString();
            item.SimpleResponse.DisplayText = Regex.Replace(sbResponse.ToString(), "<.*?>", String.Empty);
            response.Payload.Google.RichResponse.Items = new Item[] { item };

            return response;
        }
        
        public DateTime GetDateFromString(ref Parameters Params)
        {
            if (Params != null && Params.Duration != null)
            {
                var amt = Params.Duration.Amount;
                var unit = Params.Duration.Unit;

                DateTime start = DateTime.Now;
                DateTime end = DateTime.Now;

                switch (unit)
                {
                    case "h":
                        start = DateTime.Now.AddHours(-amt);
                        break;
                    case "wk":
                        start = DateTime.Now.AddDays(-(amt * 7));
                        break;
                    case "day":
                        start = DateTime.Now.AddDays(-amt);
                        break;
                    case "min":
                        start = DateTime.Now.AddMinutes(-amt);
                        break;
                    case "mo":
                        start = DateTime.Now.AddMonths(-(int)amt);
                        break;
                    case "yr":
                        start = DateTime.Now.AddYears(-(int)amt);
                        break;
                }

                Params.DurationDate.StartDate = start;
                Params.DurationDate.EndDate = end;
            }
            return DateTime.Now;
        }
    }
}
