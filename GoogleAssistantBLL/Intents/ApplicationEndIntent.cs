﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class ApplicationEndIntent : IIntentProcessor
    {
        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            item.SimpleResponse.Ssml = "<speak><s>You are welcome, Have a nice day!</s></speak>";
            response.Payload.Google.RichResponse.Items = new Item[] { item };
            response.Payload.Google.ExpectUserResponse = false;

            return response;
        }
    }
}
