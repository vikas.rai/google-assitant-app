﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class WelcomeIntent : IIntentProcessor
    {
        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            await Task.Run(() => { });
            var response = SsmlResponse.GenerateNewInstance;
            Item item = Item.GetNewInstance;
            //response.Payload.Google.ExpectUserResponse = false;
            item.SimpleResponse.Ssml = $"<speak><s>Hi {user.Name}.</s><s>Welcome back, would you like me to brief you about your brand's performance?</s></speak>";
            response.Payload.Google.RichResponse.Items = new Item[] { item };
            return response;
        }
    }
}
