﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class Brand
    {
        public long BrandID { get; set; }
        public string BrandName { get; set; }
        public string BrandFriendlyName { get; set; }
    }
}
