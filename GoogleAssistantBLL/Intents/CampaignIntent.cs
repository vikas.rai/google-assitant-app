﻿using GoogleAssistantBLL.Interfaces;
using GoogleAssistantBLL.Models;
using GoogleAssistantBLL.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GoogleAssistantBLL.Intents
{
    public class CampaignListIntent : IIntentProcessor
    {
        string connectionString = string.Empty;

        public CampaignListIntent(string ConnectionString)
        {
            connectionString = ConnectionString;
        }

        public async Task<SsmlResponse> ProcessAIRequest(DialogFlowRequest request, UserModel user)
        {
            var lstCampaigns = await GetCampaignList(user);

            StringBuilder sbResponse = new StringBuilder();

            sbResponse.AppendLine($"<speak><p><s>currently you have {lstCampaigns.Count} active campaigns.<s>");
            for (int i = 0; i < lstCampaigns.Count; i++)
            {
                sbResponse.AppendLine($"<s><say-as interpret-as=\"ordinal\">{(i+1)}</say-as> is running for around {Utils.TimeAgo(lstCampaigns[i].DateDiff)}.</s>");
            }
            sbResponse.AppendLine("</p>");
            sbResponse.AppendLine("<break time='200ms'/>");
            sbResponse.AppendLine("Is there anything else I can help you with?</speak>");

            //sbResponse.AppendLine("<s>First is running for around 15 days.</s>");
            //sbResponse.AppendLine("<s>Second is running for around 1 month.</s></p></speak>");

            var response = SsmlResponse.GenerateNewInstance;

            var resItem = new Item();
            resItem.SimpleResponse = new SimpleResponse();
            resItem.SimpleResponse.Ssml = sbResponse.ToString();
            resItem.SimpleResponse.DisplayText = Regex.Replace(sbResponse.ToString(), "<.*?>", String.Empty); ;

            response.Payload.Google.RichResponse.Items = new Item[] { resItem };
            return response;
        }

        public async Task<List<Campaign>> GetCampaignList(UserModel user)
        {
            IDataAccess db = new DatabaseAsyncWrapper();
            List<CampaignAvg> lstCampaignAvg = new List<CampaignAvg>();
            var lstCampaign = new List<Campaign>();
            var locations = new List<Location>();

            using (var transaction = await db.GetTransaction(connectionString))
            {
                using (var reader = await db.GetReader(transaction, "GoogleAssistant_CampaignList", new Dictionary<string, object>() {
                    { "@CategoryName", user.CategoryName },
                    { "@BrandName", user.Brands.First().BrandName },
                    { "@CategoryID", user.CategoryID },
                    { "@BrandID", user.Brands.First().BrandID },
                    { "@StartDate", DateTime.UtcNow.Date.AddDays(-30).ToUniversalTime() },
                    { "@EndDate", DateTime.UtcNow.Date.Add(new TimeSpan(0,23,59,59,59)).ToUniversalTime() }
                }))
                {
                    while (reader.Read())
                    {
                        var campaign = new Campaign();
                        campaign.CampaignID = Convert.ToInt64(reader["CampaignID"]);
                        campaign.CampaignName = Convert.ToString(reader["CampaignName"]);
                        campaign.StartDate = Convert.ToDateTime(reader["StartDate"]);
                        campaign.EndDate = Convert.ToDateTime(reader["Enddate"]);
                        campaign.Status = Convert.ToInt32(reader["Status"]);
                        campaign.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                        campaign.DateDiff = Convert.ToInt32(reader["DateDifference"]);
                        lstCampaign.Add(campaign);
                    }
                    if (reader != null)
                        reader.Close();
                }

                db.CloseConnection(transaction);

            }

            return lstCampaign;
        }

    }
}
